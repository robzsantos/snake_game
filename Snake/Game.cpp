#include "src/GameController.h"
#include "src/GameModel.h"
#include "src/GameView.h"

int main() {

  Snk::GameView gameView(Snk::WINDOW_DIMENSIONS.x, Snk::WINDOW_DIMENSIONS.y, Snk::WINDOW_OFFSET.x, 
    Snk::WINDOW_OFFSET.y, Snk::TEXT_WINDOW_TITLE);
  Snk::GameModel gameModel(gameView);
  Snk::GameController gameController(gameView, gameModel);

  while (gameView.windowIsOpen()) {

    if (gameModel.getCurrentState() == Snk::States::SnakeRespawn) {
      gameController.reset();
    }

    gameController.processEventsQueue();
    gameView.paintGameScreen();
    gameModel.refresh();
    gameView.refresh();
  }

  return 0;
}
