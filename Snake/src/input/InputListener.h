#pragma once

#include <SFML/Window/Event.hpp>

namespace Snk {

  class InputListener {

  public:
    virtual void onMouseDown(const sf::Event &event) = 0;

    virtual void onMouseUp(const sf::Event &event) = 0;

    virtual void onKeyDown(const sf::Event &event) = 0;

    virtual void onKeyUp(const sf::Event &event) = 0;

    virtual void onClose() = 0;

    virtual void onFocusOut() = 0;

    virtual void onFocusIn() = 0;
  };

}