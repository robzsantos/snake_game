#pragma once

#include <vector>

#include "InputListener.h"

namespace Snk {

  class InputEventsPublisher {

    std::vector<InputListener *> listeners;

  public:
    void addListener(InputListener *listener);
    
    void removeListener(const InputListener *listener);

    void publishEvent(const sf::Event &event);
  };

}


