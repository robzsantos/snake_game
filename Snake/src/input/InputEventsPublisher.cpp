#include "InputEventsPublisher.h"

namespace Snk {

  void InputEventsPublisher::addListener(InputListener *listener) {
    listeners.push_back(listener);
  }

  void InputEventsPublisher::removeListener(const InputListener *listener) {
    listeners.erase(std::remove(listeners.begin(), listeners.end(), listener),
                    listeners.end());
  }

  void InputEventsPublisher::publishEvent(const sf::Event &event) {
    for (InputListener *listener : listeners) {

      switch (event.type) {

      case sf::Event::MouseButtonPressed:
        listener->onMouseDown(event);
        break;

      case sf::Event::MouseButtonReleased:
        listener->onMouseUp(event);
        break;

      case sf::Event::Closed:
        listener->onClose();
        break;

      case sf::Event::KeyPressed:
        listener->onKeyDown(event);
        break;

      case sf::Event::KeyReleased:
        listener->onKeyUp(event);
        break;

      case sf::Event::LostFocus:
        listener->onFocusOut();
        break;

      case sf::Event::GainedFocus:
        listener->onFocusIn();
        break;

        // Uncomment to know other event and map
        // default:
        //	std::cout << "other Event: " << event.type << std::endl;
        //	break;
      }
    }
  }

} // namespace Snk
