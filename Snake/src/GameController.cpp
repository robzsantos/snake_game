#include "GameController.h"

namespace Snk {

  GameController::GameController(GameView &view, GameModel &model)
      : mView(view), mModel(model) {
    mEventsPub.addListener(this);
  }

  GameController::~GameController() {}

  void GameController::processEventsQueue() {
    sf::Event event;

    while (mView.getGameWindow().pollEvent(event)) {
      mEventsPub.publishEvent(event);
    }
  }

  void GameController::onMouseDown(const sf::Event &event) {
    if (mModel.getCurrentState() == States::StartGame) {
      reset();
      mModel.setNextState(States::SnakeMoving);
    } else if (mModel.getCurrentState() == States::EndGame ||
               mModel.getCurrentState() == States::GameOver) {
      reset();
      mModel.resetGame();
      mModel.setNextState(States::SnakeMoving);
    }
  }

  void GameController::onMouseUp(const sf::Event &event) {}

  void GameController::onKeyDown(const sf::Event &event) {

    if (!mModel.getSnake()->isWaitingMove()) {
      switch (event.key.code) {
      case sf::Keyboard::Left:
        if (mLastKeyPressed.code != sf::Keyboard::Left &&
            mLastKeyPressed.code != sf::Keyboard::Right &&
            mModel.getCurrentState() == States::SnakeMoving) {

          mModel.changeSnakeDirection(SnakeDirection::Left);
          mLastKeyPressed.code = sf::Keyboard::Left;
        }
        break;

      case sf::Keyboard::Up:
        if (mLastKeyPressed.code != sf::Keyboard::Up &&
            mLastKeyPressed.code != sf::Keyboard::Down &&
            mModel.getCurrentState() == States::SnakeMoving) {

          mModel.changeSnakeDirection(SnakeDirection::Up);
          mLastKeyPressed.code = sf::Keyboard::Up;
        }
        break;
      case sf::Keyboard::Right:
        if (mLastKeyPressed.code != sf::Keyboard::Right &&
            mLastKeyPressed.code != sf::Keyboard::Left &&
            mModel.getCurrentState() == States::SnakeMoving) {

          mModel.changeSnakeDirection(SnakeDirection::Right);
          mLastKeyPressed.code = sf::Keyboard::Right;
        }
        break;
      case sf::Keyboard::Down:
        if (mLastKeyPressed.code != sf::Keyboard::Down &&
            mLastKeyPressed.code != sf::Keyboard::Up &&
            mModel.getCurrentState() == States::SnakeMoving) {

          mModel.changeSnakeDirection(SnakeDirection::Down);
          mLastKeyPressed.code = sf::Keyboard::Down;
        }
        break;
      case sf::Keyboard::Space:
        if (mModel.getCurrentState() == States::PauseGame) {
          mModel.onContinueGame();
          mInducePaused = false;
        } else {
          mModel.onPauseGame();
          mInducePaused = true;
        }

        break;
        // uncomment to debug speed
        // case sf::Keyboard::S:
        //	mModel.increaseSpeed();
        //	break;
      }
    }
  }

  void GameController::onKeyUp(const sf::Event &event) {}

  void GameController::onClose() { mView.close(); }

  void GameController::onFocusOut() { mModel.onPauseGame(); }

  void GameController::onFocusIn() {
    if (!mInducePaused) {
      mModel.onContinueGame();
    }
  }

  void GameController::reset() { mLastKeyPressed.code = sf::Keyboard::Up; }

} // namespace Snk
