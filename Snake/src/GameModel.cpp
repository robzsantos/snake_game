#include "GameModel.h"

namespace Snk {

  GameModel::GameModel(GameView &view) : mView(view) {

    srand((unsigned)time(NULL));

    mStateStartGame = std::make_unique<StateStartGame>(this);
    mStateSnakeMoving = std::make_unique<StateSnakeMoving>(this);
    mStateSnakeDying = std::make_unique<StateSnakeDying>(this);
    mStateSnakeRespawn = std::make_unique<StateSnakeRespawn>(this);
    mStateSnakeEating = std::make_unique<StateSnakeEating>(this);
    mStateLevelUp = std::make_unique<StateLevelUp>(this);
    mStateGameOver = std::make_unique<StateGameOver>(this);
    mStateEndGame = std::make_unique<StateEndGame>(this);
    mStatePauseGame = std::make_unique<StatePauseGame>(this);

    setNextState(States::StartGame);
    resetGame();
  }

  GameModel::~GameModel() {}

  void GameModel::changeSnakeDirection(const SnakeDirection direction) {
    mSnake->changeDirection(direction);
  }

  void GameModel::createApple() {

    Lot &lot = mTerrain->getLotFree();

    int offset = static_cast<int>((lot.getSize() - GAME_APPLE_SIZE) / 2);

    mApple = std::make_unique<Apple>(lot.getPosX() + offset,
                                     lot.getPosY() + offset, GAME_APPLE_SIZE);

    lot.setOccupation(LotOccupation::Apple);
  }

  void GameModel::increaseApples() {
    ++mApplesEaten;
    mView.updateScore(mApplesEaten);
  }

  void GameModel::increaseLevel() {
    ++mLevel;
    mView.updateLevel(mLevel);
  }

  void GameModel::increaseSpeed() {
    mSnake->setSpeed(GAME_SPEED_LEVEL[mLevel - 1]);
    std::cout << "New speed: " << mSnake->getSpeed() << std::endl;
  }

  void GameModel::onPauseGame() {
    if (mCurrentStateEnum != States::EndGame &&
        mCurrentStateEnum != States::GameOver &&
        mCurrentStateEnum != States::StartGame &&
        mCurrentStateEnum != States::PauseGame)
      setNextState(States::PauseGame);
  }

  void GameModel::onContinueGame() {
    if (mCurrentStateEnum != States::EndGame &&
        mCurrentStateEnum != States::GameOver)
      setNextState(mPreviousStateEnum);
  }

  void GameModel::resetApple() { mApple.reset(); }

  void GameModel::resetTerrain() {
    mTerrain->createTerrain(GAME_TERRAIN_SIZE_LEVEL[mLevel - 1]);
  }

  void GameModel::resetGame() {

    mClock.restart();

    mApplesEaten = 0;

    mLevel = 1;

    mTerrain = std::make_unique<Terrain>(
        GAME_TERRAIN_DIMENSIONS.x, GAME_TERRAIN_DIMENSIONS.y,
        GAME_TERRAIN_LOT_SIZE, GAME_TERRAIN_SIZE_LEVEL[mLevel - 1]);

    mSnake = std::make_unique<Snake>(
        mTerrain->getNumLotsY() - GAME_SNAKE_START_INDENT_ROW_POSITION,
        mTerrain->getNumLotsX() - GAME_SNAKE_START_INDENT_COL_POSITION);

    mSnake->setSpeed(GAME_SPEED_LEVEL[mLevel - 1]);

    mSnake->setNextPositionByLot(mTerrain.get());

    createApple();

    mView.updateLevel(mLevel);
    mView.updateScore(mApplesEaten);
    mView.updateLifes(mSnake->getLifes());
  }

  void GameModel::refresh() {

    mView.paintTerrain(mTerrain->getOffsetX(), mTerrain->getOffsetY());

    mView.paintSnake(mSnake.get());

    if (mApple)
      mView.paintApple(mApple.get());

    mCurrentState->update();

    // Uncomment only to draw terrain marking
    // for (auto &column : mTerrain->getLots()) {

    //	for (auto &lot : column) {

    //		mView.paintPiece(lot);

    //	}
    //}
  }

  void GameModel::setNextState(const States state) {
    mPreviousStateEnum = mCurrentStateEnum;
    mCurrentStateEnum = state;

    if (mCurrentState != nullptr && mPreviousStateEnum != mCurrentStateEnum)
      mCurrentState->exit();

    switch (mCurrentStateEnum) {
    case States::StartGame:
      mCurrentState = mStateStartGame.get();
      mCurrentState->enter();
      break;
    case States::SnakeMoving:
      mCurrentState = mStateSnakeMoving.get();
      mCurrentState->enter();
      break;
    case States::SnakeDying:
      mCurrentState = mStateSnakeDying.get();
      mCurrentState->enter();
      break;
    case States::SnakeRespawn:
      mCurrentState = mStateSnakeRespawn.get();
      mCurrentState->enter();
      break;
    case States::SnakeEatingApple:
      mCurrentState = mStateSnakeEating.get();
      mCurrentState->enter();
      break;
    case States::LevelUp:
      mCurrentState = mStateLevelUp.get();
      mCurrentState->enter();
      break;
    case States::GameOver:
      mCurrentState = mStateGameOver.get();
      mCurrentState->enter();
      break;
    case States::EndGame:
      mCurrentState = mStateEndGame.get();
      mCurrentState->enter();
      break;
    case States::PauseGame:
      mCurrentState = mStatePauseGame.get();
      mCurrentState->enter();
      break;
    }
  }

  void GameModel::setGameOver() {
    mView.paintGameOverScreen(mApplesEaten, mLevel);
  }

  // uncomment to debug speed
  // void increaseSpeed() {
  //	mSnake->increaseSpeed();
  //	std::cout << "Nova velocidade: " << mSnake->getSpeed() << std::endl;
  //}

  bool GameModel::canMove() { return _getTimeToUpdate(); }

  int GameModel::getCurrentBodyLevel() const {
    return GAME_BODY_LENGTH_LEVEL[mLevel - 1];
  }

  States GameModel::getCurrentState() const { return mCurrentStateEnum; }

  States GameModel::getPreviousState() const { return mPreviousStateEnum; }

  int GameModel::getLevel() const { return mLevel; }

  Snake *GameModel::getSnake() const { return mSnake.get(); }

  Terrain *GameModel::getTerrain() const { return mTerrain.get(); }

  GameView &GameModel::getView() const { return mView; }

  bool GameModel::_getTimeToUpdate() {

    sf::Int32 msec = mClock.getElapsedTime().asMilliseconds();

    const int halfSecond = static_cast<int>(msec * 0.01f);

    if (halfSecond % mSnake->getSpeed() == 0 && mLastElapsedTime != halfSecond) {
      mLastElapsedTime = halfSecond;
      return true;
    }

    return false;
  }
} // namespace Snk
