#include "GameOverScreen.h"

namespace Snk {

    GameOverScreen::GameOverScreen(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset)
      : ScreenDimensions(wScreen, hScreen, wOffset, hOffset) {

      mGameOverText = TextFactory::createText(Texts::GameOverScreenMessage);
      mScoreText = TextFactory::createText(Texts::GameOverScreenScore);
      mLevelText = TextFactory::createText(Texts::GameOverScreenLevel);
      mRestartText = TextFactory::createText(Texts::GameOverScreenRestart);

      mGameOverText->setPosition(GAMEOVER_TEXT_MSG_POSITION.x,
        GAMEOVER_TEXT_MSG_POSITION.y);
      mScoreText->setPosition(GAMEOVER_TEXT_SCORE_POSITION.x,
        GAMEOVER_TEXT_SCORE_POSITION.y);
      mLevelText->setPosition(GAMEOVER_TEXT_LEVEL_POSITION.x,
        GAMEOVER_TEXT_LEVEL_POSITION.y);
      mRestartText->setPosition(GAMEOVER_TEXT_RESTART_POSITION.x,
        GAMEOVER_TEXT_RESTART_POSITION.y);

      mGameOverText->setText(TEXT_GAMEOVER_MSG);
      mRestartText->setText(TEXT_GAMEOVER_RESTART);

      m_drawBackground();
    }

    GameOverScreen::~GameOverScreen() {
      if (mGameOverText) {
        delete mGameOverText;
        mGameOverText = NULL;
      }
      if (mLevelText) {
        delete mLevelText;
        mLevelText = NULL;
      }
      if (mScoreText) {
        delete mScoreText;
        mScoreText = NULL;
      }
      if (mRestartText) {
        delete mRestartText;
        mRestartText = NULL;
      }
    }

    sf::RectangleShape &GameOverScreen::getBackground() { return mBackground; }

    sf::Text &GameOverScreen::getGameOverText() const { return mGameOverText->getDrawableText(); }
    sf::Text &GameOverScreen::getRestartText() const { return mRestartText->getDrawableText(); }

    sf::Text &GameOverScreen::getSettedUpScoreText(const int score) const {
      const std::string text =
        (score < 10) ? TEXT_GAMEOVER_SCORE + "0" : TEXT_GAMEOVER_SCORE;

      mScoreText->setText(text + std::to_string(score));

      return mScoreText->getDrawableText();
    }

    sf::Text &GameOverScreen::getSettedUpLevelText(const int level) const {
      const std::string text =
        (level < 10) ? TEXT_GAMEOVER_LEVEL + "0" : TEXT_GAMEOVER_LEVEL;

      mLevelText->setText(text + std::to_string(level));

      return mLevelText->getDrawableText();
    }

    void GameOverScreen::m_drawBackground() {
      mBackground.setSize(sf::Vector2f(mDimensions.x, mDimensions.y));
      mBackground.setFillColor(sf::Color(BLACK_35PERCENT_TRANSPARENT));
    }

}
