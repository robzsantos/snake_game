#include "GameScreen.h"

namespace Snk {

  GameScreen::GameScreen(const float wScreen, const float hScreen, const float wOffset,
                         const float hOffset)
      : ScreenDimensions(wScreen, hScreen, wOffset, hOffset) {

    mBgGameSprite = SpriteFactory::createSprite(Sprites::BackgroundGame);
    mAppleImgHud = SpriteFactory::createSprite(Sprites::Apple);
    mAppleSprite = SpriteFactory::createSprite(Sprites::Apple);

    mSnakeHeadSprite = SpriteFactory::createSprite(Sprites::Snake);
    mSnakeBodySprite = SpriteFactory::createSprite(Sprites::Snake);
    mSnakeTailSprite = SpriteFactory::createSprite(Sprites::Snake);

    mLevelText = TextFactory::createText(Texts::GameScreenLevel);
    mScoreText = TextFactory::createText(Texts::GameScreenScore);
    mLifesText = TextFactory::createText(Texts::GameScreenLifes);

    mLifesText->setPosition(GAME_TEXT_LIFE_POSITION.x, GAME_TEXT_LIFE_POSITION.y);
    mLevelText->setPosition(GAME_TEXT_LEVEL_POSITION.x,
                            GAME_TEXT_LEVEL_POSITION.y);
    mScoreText->setPosition(GAME_TEXT_SCORE_POSITION.x,
                            GAME_TEXT_SCORE_POSITION.y);
    mAppleImgHud->setPosition(GAME_SPRITE_APPLE_HUD_POSITION.x,
                              GAME_SPRITE_APPLE_HUD_POSITION.y);

    m_drawHudArea();
    m_setDefaultTerrainProperties();
  }

  GameScreen::~GameScreen() {
    if (mBgGameSprite) {
      delete mBgGameSprite;
      mBgGameSprite = NULL;
    }
    if (mAppleImgHud) {
      delete mAppleImgHud;
      mAppleImgHud = NULL;
    }
    if (mSnakeBodySprite) {
      delete mSnakeBodySprite;
      mSnakeBodySprite = NULL;
    }
    if (mSnakeHeadSprite) {
      delete mSnakeHeadSprite;
      mSnakeHeadSprite = NULL;
    }
    if (mSnakeTailSprite) {
      delete mSnakeTailSprite;
      mSnakeTailSprite = NULL;
    }
    if (mAppleSprite) {
      delete mAppleSprite;
      mAppleSprite = NULL;
    }
    if (mLevelText) {
      delete mLevelText;
      mLevelText = NULL;
    }
    if (mScoreText) {
      delete mScoreText;
      mScoreText = NULL;
    }
    if (mLifesText) {
      delete mLifesText;
      mLifesText = NULL;
    }
  }

  sf::Sprite &GameScreen::getBgGameSprite() const { return mBgGameSprite->getSprite(); }
  sf::Sprite &GameScreen::getAppleHudSprite() const {
    return mAppleImgHud->getSprite();
  }

  sf::RectangleShape &GameScreen::getHudBg() { return mHudBg; }

  sf::Text &GameScreen::getLevelText() const { return mLevelText->getDrawableText(); }
  sf::Text &GameScreen::getScoreText() const { return mScoreText->getDrawableText(); }
  sf::Text &GameScreen::getLifesText() const { return mLifesText->getDrawableText(); }

  sf::RectangleShape &GameScreen::getSettedUpTerrain(const float lvlOffsetX,
                                                     const float lvlOffsetY) {

    const float totalOffsetX = mOffset.x + lvlOffsetX;
    const float totalOffsetY = mOffset.y + lvlOffsetY;
    const float realWidth = mDimensions.x - (mOffset.x * 2 + lvlOffsetX * 2);
    const float realHeight = mDimensions.y - (mOffset.y + lvlOffsetY * 2);

    mTerrainArea.setPosition(sf::Vector2f(totalOffsetX, totalOffsetY));
    mTerrainArea.setSize(sf::Vector2f(realWidth, realHeight));

    return mTerrainArea;
  }

  // TEMP
  sf::RectangleShape GameScreen::getSettedUpPiece(const Lot &piece) const {

    sf::RectangleShape mLot;

    mLot.setPosition(mOffset.x + piece.getPosX(), mOffset.y + piece.getPosY());
    mLot.setSize(sf::Vector2f(piece.getSize(), piece.getSize()));
    if (piece.getOccupation() == LotOccupation::Free) {
      mLot.setFillColor(sf::Color(0, 0, 255, 50));
    } else {
      mLot.setFillColor(sf::Color(255, 0, 0, 50));
    }
    mLot.setOutlineColor(sf::Color::Magenta);
    mLot.setOutlineThickness(1.5f);

    // std::cout << "PosX: "<<mLot.getPosition().x << std::endl;
    // std::cout << "PosY: "<<mLot.getPosition().y << std::endl;

    return mLot;
  }

  sf::Sprite &GameScreen::getSettedUpSnakeHead(Snake *snake) const {
    int headDirection = static_cast<int>(snake->getHead()->getDirection());

    mSnakeHeadSprite->setFrame(
        sf::IntRect(headDirection * snake->getHead()->getSize(), 0,
                    snake->getHead()->getSize(), snake->getHead()->getSize()));
    mSnakeHeadSprite->setPosition(mOffset.x + snake->getHead()->getPosX(),
                                  mOffset.y + snake->getHead()->getPosY());
    mSnakeHeadSprite->setColor(
        sf::Color(255, 255, 255, snake->getHead()->getAlpha()));

    return mSnakeHeadSprite->getSprite();
  }

  sf::Sprite &GameScreen::getSettedUpSnakeTail(Snake *snake) const {
    int tailDirection = static_cast<int>(snake->getTail()->getDirection()) +
                        GAME_SPRITE_SNAKE_TAIL_FRAME_POSITION;

    mSnakeTailSprite->setFrame(
        sf::IntRect(tailDirection * snake->getTail()->getSize(), 0,
                    snake->getTail()->getSize(), snake->getTail()->getSize()));
    mSnakeTailSprite->setPosition(mOffset.x + snake->getTail()->getPosX(),
                                  mOffset.y + snake->getTail()->getPosY());
    mSnakeTailSprite->setColor(
        sf::Color(255, 255, 255, snake->getTail()->getAlpha()));

    return mSnakeTailSprite->getSprite();
  }

  sf::Sprite &GameScreen::getSettedUpSnakeBody(const SnakeBody *body) const {

    mSnakeBodySprite->setFrame(
        sf::IntRect(GAME_SPRITE_SNAKE_BODY_FRAME_POSITION * body->getSize(), 0,
                    body->getSize(), body->getSize()));
    mSnakeBodySprite->setPosition(mOffset.x + body->getPosX(),
                                  mOffset.y + body->getPosY());
    mSnakeBodySprite->setColor(sf::Color(255, 255, 255, body->getAlpha()));

    return mSnakeBodySprite->getSprite();
  }

  sf::Sprite &GameScreen::getSettedUpApple(const Apple *apple) const {

    mAppleSprite->setPosition(mOffset.x + apple->getPosX(),
                              mOffset.y + apple->getPosY());

    return mAppleSprite->getSprite();
  }

  void GameScreen::updateScore(const int score) {
    const std::string text =
        (score < 10) ? TEXT_GAME_SCORE + "0" : TEXT_GAME_SCORE;

    mScoreText->setText(text + std::to_string(score));
  }

  void GameScreen::updateLevel(const int level) {
    const std::string text =
        (level < 10) ? TEXT_GAME_LEVEL + "0" : TEXT_GAME_LEVEL;

    mLevelText->setText(text + std::to_string(level));
  }

  void GameScreen::updateLifes(const int lifes) {
    mLifesText->setText(TEXT_GAME_LIFE + std::to_string(lifes));
  }

  void GameScreen::m_drawHudArea() {
    mHudBg.setSize(sf::Vector2f(mDimensions.x, mDimensions.y));
    mHudBg.setFillColor(sf::Color(BLACK_50PERCENT_TRANSPARENT));
  }

  void GameScreen::m_setDefaultTerrainProperties() {
    mTerrainArea.setTexture(mBgGameSprite->getTexture());
    mTerrainArea.setPosition(sf::Vector2f(mOffset.x, mOffset.y));
    mTerrainArea.setOutlineColor(sf::Color::Black);
    mTerrainArea.setOutlineThickness(2.f);
  }

} // namespace Snk
