#include "PauseScreen.h"

namespace Snk {

  PauseScreen::PauseScreen(const float wScreen, const float hScreen, 
    const float wOffset, const float hOffset)
      : ScreenDimensions(wScreen, hScreen, wOffset, hOffset) {

    mPauseText = TextFactory::createText(Texts::PauseScreenMessage);
    mPauseText->setPosition(PAUSE_TEXT_MSG_POSITION.x, PAUSE_TEXT_MSG_POSITION.y);
    mPauseText->setText(TEXT_PAUSE_GAME);

    m_drawBackground();
  }

  PauseScreen::~PauseScreen() {
    if (mPauseText) {
      delete mPauseText;
      mPauseText = NULL;
    }
  }

  sf::RectangleShape &PauseScreen::getBackground() { return mBackground; }

  sf::Text &PauseScreen::getPauseText() const { return mPauseText->getDrawableText(); }

  void PauseScreen::m_drawBackground() {
    mBackground.setSize(sf::Vector2f(mDimensions.x, mDimensions.y));
    mBackground.setFillColor(BLACK_35PERCENT_TRANSPARENT);
  }

} // namespace Snk
