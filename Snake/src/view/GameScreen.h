#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "../game/Apple.h"
#include "../game/Snake.h"
#include "ScreenDimensions.h"
#include "sprite/SpriteFactory.h"
#include "text/TextFactory.h"

namespace Snk {

  class GameScreen : public ScreenDimensions {

    public:
      GameScreen(const float wScreen, const float hScreen, const float wOffset, const float hOffset);

      ~GameScreen();

      sf::Sprite &getBgGameSprite() const;
      sf::Sprite &getAppleHudSprite() const;

      sf::RectangleShape &getHudBg();

      sf::Text &getLevelText() const;
      sf::Text &getScoreText() const;
      sf::Text &getLifesText() const;

      sf::RectangleShape &getSettedUpTerrain(const float lvlOffsetX,
        const float lvlOffsetY);

      // TEMP
      sf::RectangleShape getSettedUpPiece(const Lot &piece) const;

      sf::Sprite &getSettedUpSnakeHead(Snake *snake) const;

      sf::Sprite &getSettedUpSnakeTail(Snake *snake) const;

      sf::Sprite &getSettedUpSnakeBody(const SnakeBody *body) const;

      sf::Sprite &getSettedUpApple(const Apple *apple) const;

      void updateScore(const int score);

      void updateLevel(const int level);

      void updateLifes(const int lifes);

    private:
      sf::RectangleShape mHudBg;
      sf::RectangleShape mTerrainArea;

      Sprite *mBgGameSprite = NULL;
      Sprite *mSnakeBodySprite = NULL;
      Sprite *mSnakeHeadSprite = NULL;
      Sprite *mSnakeTailSprite = NULL;
      Sprite *mAppleSprite = NULL;

      Sprite *mAppleImgHud = NULL;

      Text *mLevelText = NULL;
      Text *mScoreText = NULL;
      Text *mLifesText = NULL;

      void m_drawHudArea();
      void m_setDefaultTerrainProperties();

  };

} // namespace Snk
