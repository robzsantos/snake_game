#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "ScreenDimensions.h"
#include "text/TextFactory.h"

namespace Snk {

  class LevelUpScreen : public ScreenDimensions {

  public:
    
    LevelUpScreen(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset);

    ~LevelUpScreen();

    bool isDoingAnimation() const;

    sf::RectangleShape &getBackground();

    sf::Text &getSettedUpLetterL1();

    sf::Text &getSettedUpLetterE1() const;

    sf::Text &getSettedUpLetterV() const;

    sf::Text &getSettedUpLetterE2() const;

    sf::Text &getSettedUpLetterL2() const;

    sf::Text &getSettedUpLetterU() const;

    sf::Text &getSettedUpLetterP() const;

    sf::Text &getSettedUpExclamation();

    void reset();

  private:
    static const int MIDDLE_SCREEN = 285;
    static const int SECOND = 60;

    sf::RectangleShape mBackground;

    Text *mLetterL1 = NULL;
    Text *mLetterE1 = NULL;
    Text *mLetterV = NULL;
    Text *mLetterE2 = NULL;
    Text *mLetterL2 = NULL;
    Text *mLetterU = NULL;
    Text *mLetterP = NULL;
    Text *mExclamation = NULL;

    int mStartPosY;
    int mCount;
    float mTimeAnimation;

    bool doingAnimation;

    void m_drawBackground();
  };

} // namespace Snk
