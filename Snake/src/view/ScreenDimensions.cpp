#include "ScreenDimensions.h"

namespace Snk {

  ScreenDimensions::ScreenDimensions(const float wScreen, const float hScreen, 
    const float wOffset, const float hOffset)
    : mDimensions(wScreen, hScreen), mOffset(wOffset, hOffset) {}

  ScreenDimensions::~ScreenDimensions() {}

}
