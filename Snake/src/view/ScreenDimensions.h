#pragma once

#include <SFML/System/Vector2.hpp>

namespace Snk {

  class ScreenDimensions {
  public:
    ScreenDimensions(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset);

    ~ScreenDimensions();

  protected:
    sf::Vector2f mDimensions;
    sf::Vector2f mOffset;
  };

}
