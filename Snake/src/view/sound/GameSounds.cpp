#include "GameSounds.h"

namespace Snk {

  GameSounds::GameSounds() {

    mSnakeMoveSnd = SoundFactory::createSound(SoundEffects::SnakeMove);
    mSnakeHitSnd = SoundFactory::createSound(SoundEffects::SnakeHit);
    mSnakeEatSnd = SoundFactory::createSound(SoundEffects::SnakeEat);
    mLevelUpSnd = SoundFactory::createSound(SoundEffects::LevelUp);
    mGameOverSnd = SoundFactory::createSound(SoundEffects::GameOver);
  }

  GameSounds::~GameSounds() {
    if (mSnakeMoveSnd) {
      delete mSnakeMoveSnd;
      mSnakeMoveSnd = NULL;
    }
    if (mSnakeHitSnd) {
      delete mSnakeHitSnd;
      mSnakeHitSnd = NULL;
    }
    if (mSnakeEatSnd) {
      delete mSnakeEatSnd;
      mSnakeEatSnd = NULL;
    }
    if (mLevelUpSnd) {
      delete mLevelUpSnd;
      mLevelUpSnd = NULL;
    }
    if (mGameOverSnd) {
      delete mGameOverSnd;
      mGameOverSnd = NULL;
    }
  }

  void GameSounds::playSnakeMoveSound() { mSnakeMoveSnd->play(); }

  void GameSounds::playSnakeEatSound() { mSnakeEatSnd->play(); }

  void GameSounds::playSnakeHitSound() { mSnakeHitSnd->play(); }

  void GameSounds::playLevelUpSound() { mLevelUpSnd->play(); }

  void GameSounds::playGameOverSound() { mGameOverSnd->play(); }

  void GameSounds::stopSnakeMoveSound() { mSnakeMoveSnd->stop(); }

} // namespace Snk
