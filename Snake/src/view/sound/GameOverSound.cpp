#include "GameOverSound.h"

namespace Snk {

  void GameOverSound::loadSound(const std::string &path) {

    if (!buffer.loadFromFile(path))
      std::cout << "Error to load sound: " << path << std::endl;
  }

  void GameOverSound::setSound() { sound.setBuffer(buffer); }

  void GameOverSound::play() { sound.play(); }

  void GameOverSound::stop() { sound.stop(); }

}
