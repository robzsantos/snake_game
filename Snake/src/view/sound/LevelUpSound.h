#pragma once

#include <iostream>

#include "Sound.h"

namespace Snk {

  class LevelUpSound : public Sound {

  public:
    void loadSound(const std::string &path);

    void setSound();

    void play();

    void stop();
  };

}
