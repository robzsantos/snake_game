#include "SnakeDeathSound.h"

namespace Snk {

  void SnakeDeathSound::loadSound(const std::string &path) {

    if (!buffer.loadFromFile(path))
      std::cout << "Error to load sound: " << path << std::endl;
  }

  void SnakeDeathSound::setSound() { sound.setBuffer(buffer); }

  void SnakeDeathSound::play() { sound.play(); }

  void SnakeDeathSound::stop() { sound.stop(); }

}
