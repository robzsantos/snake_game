#include "SoundFactory.h"

namespace Snk {

  Sound *SoundFactory::createSound(const SoundEffects type) {

    Sound *sound = NULL;

    switch (type) {
    case SoundEffects::SnakeMove:
      sound = new SnakeMoveSound;
      sound->loadSound(PATH_SND_SNAKE_MOVING);
      break;

    case SoundEffects::SnakeEat:
      sound = new SnakeEatSound;
      sound->loadSound(PATH_SND_SNAKE_EATING);
      break;

    case SoundEffects::SnakeHit:
      sound = new SnakeHitSound;
      sound->loadSound(PATH_SND_SNAKE_DIYNG);
      break;

    case SoundEffects::LevelUp:
      sound = new LevelUpSound;
      sound->loadSound(PATH_SND_LEVEL_UP);
      break;

    case SoundEffects::GameOver:
      sound = new GameOverSound;
      sound->loadSound(PATH_SND_GAME_OVER);
      break;

    default:
      std::cout << "Invalid sound!" << std::endl;
      return NULL;
      break;
    }

    sound->setSound();

    return sound;
  }

} // namespace Snk
