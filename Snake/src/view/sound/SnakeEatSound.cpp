#include "SnakeEatSound.h"

namespace Snk {

  void SnakeEatSound::loadSound(const std::string &path) {

    if (!buffer.loadFromFile(path))
      std::cout << "Error to load sound: " << path << std::endl;
  }

  void SnakeEatSound::setSound() { sound.setBuffer(buffer); }

  void SnakeEatSound::play() { sound.play(); }

  void SnakeEatSound::stop() { sound.stop(); }

}
