#pragma once

#include <iostream>

#include "../../Constants.h"
#include "../../Enums.h"
#include "GameOverSound.h"
#include "LevelUpSound.h"
#include "SnakeEatSound.h"
#include "SnakeHitSound.h"
#include "SnakeMoveSound.h"

namespace Snk {

  class SoundFactory {

  public:
    static Sound *createSound(const SoundEffects type);

  };
}
