#include "SnakeMoveSound.h"

namespace Snk {

  void SnakeMoveSound::loadSound(const std::string &path) {

    if (!buffer.loadFromFile(path))
      std::cout << "Error to load sound: " << path << std::endl;
  }

  void SnakeMoveSound::setSound() { sound.setBuffer(buffer); }

  void SnakeMoveSound::play() { sound.play(); }

  void SnakeMoveSound::stop() { sound.stop(); }

}
