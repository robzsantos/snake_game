#include "LevelUpSound.h"

namespace Snk {

  void LevelUpSound::loadSound(const std::string &path) {

    if (!buffer.loadFromFile(path))
      std::cout << "Error to load sound: " << path << std::endl;
  }

  void LevelUpSound::setSound() { sound.setBuffer(buffer); }

  void LevelUpSound::play() { sound.play(); }

  void LevelUpSound::stop() { sound.stop(); }

}
