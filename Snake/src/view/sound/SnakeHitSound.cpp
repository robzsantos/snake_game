#include "SnakeHitSound.h"

namespace Snk {

  void SnakeHitSound::loadSound(const std::string &path) {

    if (!buffer.loadFromFile(path))
      std::cout << "Error to load sound: " << path << std::endl;
  }

  void SnakeHitSound::setSound() { sound.setBuffer(buffer); }

  void SnakeHitSound::play() { sound.play(); }

  void SnakeHitSound::stop() { sound.stop(); }

}
