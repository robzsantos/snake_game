#pragma once

#include "SoundFactory.h"

namespace Snk {

  class GameSounds {

  public:
    GameSounds();

    ~GameSounds();

    void playSnakeMoveSound();

    void playSnakeEatSound();

    void playSnakeHitSound();

    void playLevelUpSound();

    void playGameOverSound();

    void stopSnakeMoveSound();

  private:
    Sound *mSnakeMoveSnd = NULL;
    Sound *mSnakeEatSnd = NULL;
    Sound *mSnakeHitSnd = NULL;
    Sound *mLevelUpSnd = NULL;
    Sound *mGameOverSnd = NULL;
  };

} // namespace Snk
