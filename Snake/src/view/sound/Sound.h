#pragma once

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

namespace Snk {

  class Sound {

  public:
    virtual void loadSound(const std::string &path) = 0;

    virtual void setSound() = 0;

    virtual void play() = 0;

    virtual void stop() = 0;

  protected:
    sf::SoundBuffer buffer;
    sf::Sound sound;

  };

}
