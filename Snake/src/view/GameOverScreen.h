#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "ScreenDimensions.h"
#include "text/TextFactory.h"

namespace Snk {

  class GameOverScreen : public ScreenDimensions {

  public:
    GameOverScreen(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset);

    ~GameOverScreen();

    sf::RectangleShape &getBackground();

    sf::Text &getGameOverText() const;
    sf::Text &getRestartText() const;

    sf::Text &getSettedUpScoreText(const int score) const;

    sf::Text &getSettedUpLevelText(const int level) const;

  private:
    sf::RectangleShape mBackground;

    Text *mGameOverText = NULL;
    Text *mScoreText = NULL;
    Text *mLevelText = NULL;
    Text *mRestartText = NULL;

    void m_drawBackground();

  };

} // namespace Snk
