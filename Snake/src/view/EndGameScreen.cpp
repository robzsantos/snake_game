#include "EndGameScreen.h"

namespace Snk {

  EndGameScreen::EndGameScreen(const float wScreen, const float hScreen, 
    const float wOffset, const float hOffset)
      : ScreenDimensions(wScreen, hScreen, wOffset, hOffset) {

    mEndGameMessage = TextFactory::createText(Texts::GameOverScreenMessage);
    mRestartText = TextFactory::createText(Texts::GameOverScreenRestart);

    mEndGameMessage->setPosition(ENDGAME_TEXT_MSG_POSITION.x,
                                 ENDGAME_TEXT_MSG_POSITION.y);
    mRestartText->setPosition(ENDGAME_TEXT_RESTART_POSITION.x,
                              ENDGAME_TEXT_RESTART_POSITION.y);

    mEndGameMessage->setText(TEXT_ENDGAME_MSG);
    mRestartText->setText(TEXT_GAMEOVER_RESTART);

    m_drawBackground();
  }

  EndGameScreen::~EndGameScreen() {
    if (mEndGameMessage) {
      delete mEndGameMessage;
      mEndGameMessage = NULL;
    }
    if (mRestartText) {
      delete mRestartText;
      mRestartText = NULL;
    }
  }

  sf::RectangleShape &EndGameScreen::getBackground() { return mBackground; }

  sf::Text &EndGameScreen::getEndGameMessageText() const {
    return mEndGameMessage->getDrawableText();
  }
  sf::Text &EndGameScreen::getRestartText() const {
    return mRestartText->getDrawableText();
  }

  void EndGameScreen::m_drawBackground() {
    mBackground.setSize(sf::Vector2f(mDimensions.x, mDimensions.y));
    mBackground.setFillColor(sf::Color(BLACK_35PERCENT_TRANSPARENT));
  }

} // namespace Snk
