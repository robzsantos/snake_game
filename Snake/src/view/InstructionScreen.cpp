#include "InstructionsScreen.h"

namespace Snk {

  InstructionsScreen::InstructionsScreen(const float wScreen, const float hScreen,
                                         const float wOffset, const float hOffset)
      : ScreenDimensions(wScreen, hScreen, wOffset, hOffset) {

    mArrowsSprite = SpriteFactory::createSprite(Sprites::ArrowsKb);
    mSpaceSprite = SpriteFactory::createSprite(Sprites::SpaceKb);

    mArrowsSprite->setPosition(INSTRUCTIONS_SPRITE_ARROW_POSITION.x,
                               INSTRUCTIONS_SPRITE_ARROW_POSITION.y);
    mSpaceSprite->setPosition(INSTRUCTIONS_SPRITE_SPACE_POSITION.x,
                              INSTRUCTIONS_SPRITE_SPACE_POSITION.y);

    mArrowsText = TextFactory::createText(Texts::InstructionScreenArrows);
    mSpaceText = TextFactory::createText(Texts::InstructionScreenSpace);
    mWarningText = TextFactory::createText(Texts::InstructionScreenWarning);
    mCreditsText = TextFactory::createText(Texts::InstructionScreenCredits);

    mArrowsText->setPosition(INSTRUCTIONS_TEXT_ARROW_POSITION.x,
                             INSTRUCTIONS_TEXT_ARROW_POSITION.y);
    mSpaceText->setPosition(INSTRUCTIONS_TEXT_SPACE_POSITION.x,
                            INSTRUCTIONS_TEXT_SPACE_POSITION.y);
    mWarningText->setPosition(INSTRUCTIONS_TEXT_MSG_POSITION.x,
                              INSTRUCTIONS_TEXT_MSG_POSITION.y);
    mCreditsText->setPosition(INSTRUCTIONS_TEXT_CREDITS_POSITION.x,
                              INSTRUCTIONS_TEXT_CREDITS_POSITION.y);

    mArrowsText->setText(TEXT_INSTRUCTIONS_ARROW);
    mSpaceText->setText(TEXT_INSTRUCTIONS_SPACE);
    mWarningText->setText(TEXT_INSTRUCTIONS_MSG);
    mCreditsText->setText(TEXT_INSTRUCTIONS_CREDITS);

    m_drawBackground();
  }

  InstructionsScreen::~InstructionsScreen() {
    if (mArrowsSprite) {
      delete mArrowsSprite;
      mArrowsSprite = NULL;
    }
    if (mSpaceSprite) {
      delete mSpaceSprite;
      mSpaceSprite = NULL;
    }
    if (mArrowsText) {
      delete mArrowsText;
      mArrowsText = NULL;
    }
    if (mSpaceText) {
      delete mSpaceText;
      mSpaceText = NULL;
    }
    if (mWarningText) {
      delete mWarningText;
      mWarningText = NULL;
    }
    if (mCreditsText) {
      delete mCreditsText;
      mCreditsText = NULL;
    }
  }

  sf::RectangleShape &InstructionsScreen::getBackground() { return mBackground; }

  sf::Sprite &InstructionsScreen::getArrowsSprite() const {
    return mArrowsSprite->getSprite();
  }
  sf::Sprite &InstructionsScreen::getSpaceSprite() const {
    return mSpaceSprite->getSprite();
  }

  sf::Text &InstructionsScreen::getArrowsText() const {
    return mArrowsText->getDrawableText();
  }
  sf::Text &InstructionsScreen::getSpaceText() const {
    return mSpaceText->getDrawableText();
  }
  sf::Text &InstructionsScreen::getWarningText() const {
    return mWarningText->getDrawableText();
  }
  sf::Text &InstructionsScreen::getCreditsText() const {
    return mCreditsText->getDrawableText();
  }

  void InstructionsScreen::m_drawBackground() {
    mBackground.setSize(sf::Vector2f(mDimensions.x, mDimensions.y));
    mBackground.setFillColor(sf::Color(BLACK_35PERCENT_TRANSPARENT));
  }

} // namespace Snk
