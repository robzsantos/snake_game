#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "ScreenDimensions.h"
#include "text/TextFactory.h"

namespace Snk {

  class EndGameScreen : public ScreenDimensions {

  public:
    EndGameScreen(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset);

    ~EndGameScreen();

    sf::RectangleShape &getBackground();

    sf::Text &getEndGameMessageText() const;

    sf::Text &getRestartText() const;

  private:
    sf::RectangleShape mBackground;

    Text *mEndGameMessage = NULL;
    Text *mRestartText = NULL;

    void m_drawBackground();
  };

}
