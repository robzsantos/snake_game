#include "BackgroundSprite.h"

namespace Snk {

  void BackgroundSprite::loadTexture(const std::string &path) {

    if (!texture.loadFromFile(path))
      std::cout << "Error to load image: " << path << std::endl;
  }

  void BackgroundSprite::applyTexture() { sprite.setTexture(texture); }

  void BackgroundSprite::setPosition(const float x, const float y) {}

  void BackgroundSprite::setFrame(const sf::IntRect &rect) {}

  void BackgroundSprite::setColor(const sf::Color &color) {}

  void BackgroundSprite::show() {}

  void BackgroundSprite::hide() {}

  sf::Sprite &BackgroundSprite::getSprite() { return sprite; }

  sf::Texture *BackgroundSprite::getTexture() { return &texture; }

} // namespace Snk
