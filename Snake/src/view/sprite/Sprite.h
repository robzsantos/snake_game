#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

namespace Snk {

  class Sprite {

  public:
    virtual sf::Sprite &getSprite() = 0;

    virtual sf::Texture *getTexture() = 0;

    virtual void loadTexture(const std::string &path) = 0;

    virtual void applyTexture() = 0;

    virtual void setPosition(const float x, const float y) = 0;

    virtual void setFrame(const sf::IntRect &rect) = 0;

    virtual void setColor(const sf::Color &color) = 0;

    virtual void show() = 0;

    virtual void hide() = 0;

  protected:
    sf::Sprite sprite;
    sf::Texture texture;

  };

}
