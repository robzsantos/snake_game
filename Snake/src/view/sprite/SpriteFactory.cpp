#include "SpriteFactory.h"

namespace Snk {

  Sprite *SpriteFactory::createSprite(const Sprites type) {

    Sprite *sprite = NULL;

    switch (type) {
    case Sprites::BackgroundGame:
      sprite = new BackgroundSprite;
      sprite->loadTexture(PATH_IMG_BACKGROUND);
      break;

    case Sprites::Apple:
      sprite = new AppleSprite;
      sprite->loadTexture(PATH_IMG_APPLE);
      break;

    case Sprites::Snake:
      sprite = new SnakeSprite;
      sprite->loadTexture(PATH_IMG_SNAKE);
      break;

    case Sprites::ArrowsKb:
      sprite = new ArrowsInstScreenSprite;
      sprite->loadTexture(PATH_IMG_ARROW_KEYS);
      break;

    case Sprites::SpaceKb:
      sprite = new SpaceInstScreenSprite;
      sprite->loadTexture(PATH_IMG_SPACE_KEY);
      break;

    default:
      std::cout << "Invalid sprite!" << std::endl;
      return NULL;
      break;
    }

    sprite->applyTexture();

    return sprite;
  }
} // namespace Snk
