#pragma once

#include <iostream>

#include "Sprite.h"

namespace Snk {

  class BackgroundSprite : public Sprite {

  public:
    void loadTexture(const std::string &path);

    void applyTexture();

    void setPosition(const float x, const float y);

    void setFrame(const sf::IntRect &rect);

    void setColor(const sf::Color &color);

    void show();

    void hide();

    sf::Sprite &getSprite();

    sf::Texture *getTexture();
  };

} // namespace Snk
