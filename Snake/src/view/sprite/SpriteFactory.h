#pragma once

#include <iostream>

#include "../../Constants.h"
#include "../../Enums.h"
#include "AppleSprite.h"
#include "ArrowsInstScreenSprite.h"
#include "BackgroundSprite.h"
#include "SnakeSprite.h"
#include "SpaceInstScreenSprite.h"

namespace Snk {

  class SpriteFactory {

  public:
    static Sprite *createSprite(const Sprites type);
  };

}
