#include "ArrowsInstScreenSprite.h"

namespace Snk {

  void ArrowsInstScreenSprite::loadTexture(const std::string &path) {

    if (!texture.loadFromFile(path))
      std::cout << "Error to load image: " << path << std::endl;
  }

  void ArrowsInstScreenSprite::applyTexture() { sprite.setTexture(texture); }

  void ArrowsInstScreenSprite::setPosition(const float x, const float y) {
    sprite.setPosition(x, y);
  }

  void ArrowsInstScreenSprite::setFrame(const sf::IntRect &rect) {}

  void ArrowsInstScreenSprite::setColor(const sf::Color &color) {}

  void ArrowsInstScreenSprite::show() {}

  void ArrowsInstScreenSprite::hide() {}

  sf::Sprite &ArrowsInstScreenSprite::getSprite() { return sprite; }

  sf::Texture *ArrowsInstScreenSprite::getTexture() { return &texture; }

} // namespace Snk
