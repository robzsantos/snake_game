#include "AppleSprite.h"

namespace Snk {

  void AppleSprite::loadTexture(const std::string &path) {

    if (!texture.loadFromFile(path))
      std::cout << "Error to load image: " << path << std::endl;
  }

  void AppleSprite::applyTexture() { sprite.setTexture(texture); }

  void AppleSprite::setPosition(const float x, const float y) {
    sprite.setPosition(x, y);
  }

  void AppleSprite::setFrame(const sf::IntRect &rect) {}

  void AppleSprite::setColor(const sf::Color &color) { sprite.setColor(color); }

  void AppleSprite::show() {}

  void AppleSprite::hide() {}

  sf::Sprite &AppleSprite::getSprite() { return sprite; }

  sf::Texture *AppleSprite::getTexture() { return &texture; }

} // namespace Snk
