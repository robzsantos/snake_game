#include "SpaceInstScreenSprite.h"

namespace Snk {

  void SpaceInstScreenSprite::loadTexture(const std::string &path) {

    if (!texture.loadFromFile(path))
      std::cout << "Error to load image: " << path << std::endl;
  }

  void SpaceInstScreenSprite::applyTexture() { sprite.setTexture(texture); }

  void SpaceInstScreenSprite::setPosition(const float x, const float y) {
    sprite.setPosition(x, y);
  }

  void SpaceInstScreenSprite::setFrame(const sf::IntRect &rect) {}

  void SpaceInstScreenSprite::setColor(const sf::Color &color) {}

  void SpaceInstScreenSprite::show() {}

  void SpaceInstScreenSprite::hide() {}

  sf::Sprite &SpaceInstScreenSprite::getSprite() { return sprite; }

  sf::Texture *SpaceInstScreenSprite::getTexture() { return &texture; }

} // namespace Snk
