#include "SnakeSprite.h"

namespace Snk {

  void SnakeSprite::loadTexture(const std::string &path) {

    if (!texture.loadFromFile(path))
      std::cout << "Error to load image: " << path << std::endl;
  }

  void SnakeSprite::applyTexture() { sprite.setTexture(texture); }

  void SnakeSprite::setPosition(const float x, const float y) {
    sprite.setPosition(x, y);
  }

  void SnakeSprite::setFrame(const sf::IntRect &rect) {
    sprite.setTextureRect(rect);
  }

  void SnakeSprite::setColor(const sf::Color &color) { sprite.setColor(color); }

  void SnakeSprite::show() {}

  void SnakeSprite::hide() {}

  sf::Sprite &SnakeSprite::getSprite() { return sprite; }

  sf::Texture *SnakeSprite::getTexture() { return &texture; }

} // namespace Snk
