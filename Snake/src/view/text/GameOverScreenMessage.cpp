#include "GameOverScreenMessage.h"

namespace Snk {

  GameOverScreenMessage::GameOverScreenMessage() {
    fontSize = 42;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  GameOverScreenMessage::~GameOverScreenMessage() {}

  void GameOverScreenMessage::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void GameOverScreenMessage::applyFont() { text.setFont(font); }

  void GameOverScreenMessage::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void GameOverScreenMessage::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void GameOverScreenMessage::setFontSize(const int size) {}

  void GameOverScreenMessage::setFillColor(const sf::Color &color) {}

  void GameOverScreenMessage::setOutlineColor(const sf::Color &color) {}

  void GameOverScreenMessage::setLetterSpacing(const float spacing) {}

  void GameOverScreenMessage::setOutlineThickness(const float thickness) {}

  void GameOverScreenMessage::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &GameOverScreenMessage::getDrawableText() { return text; }

} // namespace Snk
