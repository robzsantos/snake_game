#include "GameOverScreenScore.h"

namespace Snk {

  GameOverScreenScore::GameOverScreenScore() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  GameOverScreenScore::~GameOverScreenScore() {}

  void GameOverScreenScore::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void GameOverScreenScore::applyFont() { text.setFont(font); }

  void GameOverScreenScore::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void GameOverScreenScore::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void GameOverScreenScore::setFontSize(const int size) {}

  void GameOverScreenScore::setFillColor(const sf::Color &color) {}

  void GameOverScreenScore::setOutlineColor(const sf::Color &color) {}

  void GameOverScreenScore::setLetterSpacing(const float spacing) {}

  void GameOverScreenScore::setOutlineThickness(const float thickness) {}

  void GameOverScreenScore::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &GameOverScreenScore::getDrawableText() { return text; }

} // namespace Snk
