#include "InstructionsScreenWarning.h"

namespace Snk {

  InstructionsScreenWarning::InstructionsScreenWarning() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  InstructionsScreenWarning::~InstructionsScreenWarning() {}

  void InstructionsScreenWarning::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void InstructionsScreenWarning::applyFont() { text.setFont(font); }

  void InstructionsScreenWarning::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void InstructionsScreenWarning::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void InstructionsScreenWarning::setFontSize(const int size) {
    text.setCharacterSize(size);
  }

  void InstructionsScreenWarning::setFillColor(const sf::Color &color) {
    text.setFillColor(color);
  }

  void InstructionsScreenWarning::setOutlineColor(const sf::Color &color) {
    text.setOutlineColor(color);
  }

  void InstructionsScreenWarning::setLetterSpacing(const float spacing) {
    text.setLetterSpacing(spacing);
  }

  void InstructionsScreenWarning::setOutlineThickness(const float thickness) {
    text.setOutlineThickness(thickness);
  }

  void InstructionsScreenWarning::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &InstructionsScreenWarning::getDrawableText() { return text; }

} // namespace Snk
