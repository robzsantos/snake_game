#pragma once

#include <iostream>

#include "Text.h"

namespace Snk {

  class GameOverScreenLevel : public Text {

  public:
    GameOverScreenLevel();

    ~GameOverScreenLevel();

    void loadFont(const std::string &path);

    void applyFont();

    void setDefault();

    void setPosition(const float x, const float y);

    void setFontSize(const int size);

    void setFillColor(const sf::Color &color);

    void setOutlineColor(const sf::Color &color);

    void setLetterSpacing(const float spacing);

    void setOutlineThickness(const float thickness);

    void setText(const std::string &string);

    sf::Text &getDrawableText();
  };

} // namespace Snk
