#include "GameOverScreenLevel.h"

namespace Snk {

  GameOverScreenLevel::GameOverScreenLevel() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  GameOverScreenLevel::~GameOverScreenLevel() {}

  void GameOverScreenLevel::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void GameOverScreenLevel::applyFont() { text.setFont(font); }

  void GameOverScreenLevel::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void GameOverScreenLevel::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void GameOverScreenLevel::setFontSize(const int size) {}

  void GameOverScreenLevel::setFillColor(const sf::Color &color) {}

  void GameOverScreenLevel::setOutlineColor(const sf::Color &color) {}

  void GameOverScreenLevel::setLetterSpacing(const float spacing) {}

  void GameOverScreenLevel::setOutlineThickness(const float thickness) {}

  void GameOverScreenLevel::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &GameOverScreenLevel::getDrawableText() { return text; }

} // namespace Snk
