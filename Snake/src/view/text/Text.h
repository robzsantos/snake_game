#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Text.hpp>

namespace Snk {

class Text {

  public:
    virtual sf::Text &getDrawableText() = 0;

    virtual void loadFont(const std::string &path) = 0;

    virtual void applyFont() = 0;

    virtual void setDefault() = 0;

    virtual void setPosition(const float x, const float y) = 0;

    virtual void setFontSize(const int size) = 0;

    virtual void setFillColor(const sf::Color &color) = 0;

    virtual void setOutlineColor(const sf::Color &color) = 0;

    virtual void setLetterSpacing(const float spacing) = 0;

    virtual void setOutlineThickness(const float thickness) = 0;

    virtual void setText(const std::string &string) = 0;

  protected:
    sf::Font font;
    sf::Text text;

    int fontSize;
    sf::Color fillColor;
    sf::Color outlineColor;
    float letterSpacing;
    float outlineThickness;
  };

} // namespace Snk
