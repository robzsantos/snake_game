#include "InstructionsScreenCredits.h"

namespace Snk {

  InstructionsScreenCredits::InstructionsScreenCredits() {
    fontSize = 14;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 1.0f;
    letterSpacing = 2.5f;
  }

  InstructionsScreenCredits::~InstructionsScreenCredits() {}

  void InstructionsScreenCredits::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void InstructionsScreenCredits::applyFont() { text.setFont(font); }

  void InstructionsScreenCredits::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void InstructionsScreenCredits::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void InstructionsScreenCredits::setFontSize(const int size) {
    text.setCharacterSize(size);
  }

  void InstructionsScreenCredits::setFillColor(const sf::Color &color) {
    text.setFillColor(color);
  }

  void InstructionsScreenCredits::setOutlineColor(const sf::Color &color) {
    text.setOutlineColor(color);
  }

  void InstructionsScreenCredits::setLetterSpacing(const float spacing) {
    text.setLetterSpacing(spacing);
  }

  void InstructionsScreenCredits::setOutlineThickness(const float thickness) {
    text.setOutlineThickness(thickness);
  }

  void InstructionsScreenCredits::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &InstructionsScreenCredits::getDrawableText() { return text; }

} // namespace Snk
