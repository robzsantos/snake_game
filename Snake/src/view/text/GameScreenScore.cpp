#include "GameScreenScore.h"

namespace Snk {

  GameScreenScore::GameScreenScore() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  GameScreenScore::~GameScreenScore() { }

  void GameScreenScore::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void GameScreenScore::applyFont() { text.setFont(font); }

  void GameScreenScore::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void GameScreenScore::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void GameScreenScore::setFontSize(const int size) {
    text.setCharacterSize(size);
  }

  void GameScreenScore::setFillColor(const sf::Color &color) {
    text.setFillColor(color);
  }

  void GameScreenScore::setOutlineColor(const sf::Color &color) {
    text.setOutlineColor(color);
  }

  void GameScreenScore::setLetterSpacing(const float spacing) {
    text.setLetterSpacing(spacing);
  }

  void GameScreenScore::setOutlineThickness(const float thickness) {
    text.setOutlineThickness(thickness);
  }

  void GameScreenScore::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &GameScreenScore::getDrawableText() { return text; }

} // namespace Snk
