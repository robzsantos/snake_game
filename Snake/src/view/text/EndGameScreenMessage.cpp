#include "EndGameScreenMessage.h"

namespace Snk {

  EndGameScreenMessage::EndGameScreenMessage() {
    fontSize = 42;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  EndGameScreenMessage::~EndGameScreenMessage() {}

  void EndGameScreenMessage::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void EndGameScreenMessage::applyFont() { text.setFont(font); }

  void EndGameScreenMessage::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void EndGameScreenMessage::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void EndGameScreenMessage::setFontSize(const int size) {}

  void EndGameScreenMessage::setFillColor(const sf::Color &color) {}

  void EndGameScreenMessage::setOutlineColor(const sf::Color &color) {}

  void EndGameScreenMessage::setLetterSpacing(const float spacing) {}

  void EndGameScreenMessage::setOutlineThickness(const float thickness) {}

  void EndGameScreenMessage::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &EndGameScreenMessage::getDrawableText() { return text; }

} // namespace Snk
