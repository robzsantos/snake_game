#include "PauseScreenMessage.h"

namespace Snk {

  PauseScreenMessage::PauseScreenMessage() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  PauseScreenMessage::~PauseScreenMessage() {}

  void PauseScreenMessage::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void PauseScreenMessage::applyFont() { text.setFont(font); }

  void PauseScreenMessage::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void PauseScreenMessage::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void PauseScreenMessage::setFontSize(const int size) {}

  void PauseScreenMessage::setFillColor(const sf::Color &color) {}

  void PauseScreenMessage::setOutlineColor(const sf::Color &color) {}

  void PauseScreenMessage::setLetterSpacing(const float spacing) {}

  void PauseScreenMessage::setOutlineThickness(const float thickness) {}

  void PauseScreenMessage::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &PauseScreenMessage::getDrawableText() { return text; }

} // namespace Snk
