#include "InstructionsScreenArrows.h"

namespace Snk {

  InstructionsScreenArrows::InstructionsScreenArrows() {
    fontSize = 18;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  InstructionsScreenArrows::~InstructionsScreenArrows() {}

  void InstructionsScreenArrows::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void InstructionsScreenArrows::applyFont() { text.setFont(font); }

  void InstructionsScreenArrows::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void InstructionsScreenArrows::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void InstructionsScreenArrows::setFontSize(const int size) {
    text.setCharacterSize(size);
  }

  void InstructionsScreenArrows::setFillColor(const sf::Color &color) {
    text.setFillColor(color);
  }

  void InstructionsScreenArrows::setOutlineColor(const sf::Color &color) {
    text.setOutlineColor(color);
  }

  void InstructionsScreenArrows::setLetterSpacing(const float spacing) {
    text.setLetterSpacing(spacing);
  }

  void InstructionsScreenArrows::setOutlineThickness(const float thickness) {
    text.setOutlineThickness(thickness);
  }

  void InstructionsScreenArrows::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &InstructionsScreenArrows::getDrawableText() { return text; }

} // namespace Snk
