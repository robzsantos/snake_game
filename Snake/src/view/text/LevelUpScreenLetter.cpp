#include "LevelUpScreenLetter.h"

namespace Snk {

  LevelUpScreenLetter::LevelUpScreenLetter() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  LevelUpScreenLetter::~LevelUpScreenLetter() {}

  void LevelUpScreenLetter::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void LevelUpScreenLetter::applyFont() { text.setFont(font); }

  void LevelUpScreenLetter::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void LevelUpScreenLetter::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void LevelUpScreenLetter::setFontSize(const int size) {}

  void LevelUpScreenLetter::setFillColor(const sf::Color &color) {}

  void LevelUpScreenLetter::setOutlineColor(const sf::Color &color) {}

  void LevelUpScreenLetter::setLetterSpacing(const float spacing) {}

  void LevelUpScreenLetter::setOutlineThickness(const float thickness) {}

  void LevelUpScreenLetter::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &LevelUpScreenLetter::getDrawableText() { return text; }

} // namespace Snk
