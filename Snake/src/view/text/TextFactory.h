#pragma once

#include <iostream>

#include "../../Constants.h"
#include "../../Enums.h"
#include "EndGameScreenMessage.h"
#include "GameOverScreenLevel.h"
#include "GameOverScreenMessage.h"
#include "GameOverScreenRestart.h"
#include "GameOverScreenScore.h"
#include "GameScreenLevel.h"
#include "GameScreenLifes.h"
#include "GameScreenScore.h"
#include "InstructionsScreenArrows.h"
#include "InstructionsScreenCredits.h"
#include "InstructionsScreenSpace.h"
#include "InstructionsScreenWarning.h"
#include "LevelUpScreenLetter.h"
#include "PauseScreenMessage.h"

namespace Snk {

  class TextFactory {

  public:
    static Text *createText(const Texts type);
  };

}
