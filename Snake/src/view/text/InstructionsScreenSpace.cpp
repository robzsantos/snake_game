#include "InstructionsScreenSpace.h"

namespace Snk {

  InstructionsScreenSpace::InstructionsScreenSpace() {
    fontSize = 18;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  InstructionsScreenSpace::~InstructionsScreenSpace() {}

  void InstructionsScreenSpace::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void InstructionsScreenSpace::applyFont() { text.setFont(font); }

  void InstructionsScreenSpace::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void InstructionsScreenSpace::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void InstructionsScreenSpace::setFontSize(const int size) {
    text.setCharacterSize(size);
  }

  void InstructionsScreenSpace::setFillColor(const sf::Color &color) {
    text.setFillColor(color);
  }

  void InstructionsScreenSpace::setOutlineColor(const sf::Color &color) {
    text.setOutlineColor(color);
  }

  void InstructionsScreenSpace::setLetterSpacing(const float spacing) {
    text.setLetterSpacing(spacing);
  }

  void InstructionsScreenSpace::setOutlineThickness(const float thickness) {
    text.setOutlineThickness(thickness);
  }

  void InstructionsScreenSpace::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &InstructionsScreenSpace::getDrawableText() { return text; }

} // namespace Snk
