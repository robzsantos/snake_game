#include "GameOverScreenRestart.h"

namespace Snk {

  GameOverScreenRestart::GameOverScreenRestart() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  GameOverScreenRestart::~GameOverScreenRestart() {}

  void GameOverScreenRestart::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void GameOverScreenRestart::applyFont() { text.setFont(font); }

  void GameOverScreenRestart::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void GameOverScreenRestart::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void GameOverScreenRestart::setFontSize(const int size) {}

  void GameOverScreenRestart::setFillColor(const sf::Color &color) {}

  void GameOverScreenRestart::setOutlineColor(const sf::Color &color) {}

  void GameOverScreenRestart::setLetterSpacing(const float spacing) {}

  void GameOverScreenRestart::setOutlineThickness(const float thickness) {}

  void GameOverScreenRestart::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &GameOverScreenRestart::getDrawableText() { return text; }

} // namespace Snk
