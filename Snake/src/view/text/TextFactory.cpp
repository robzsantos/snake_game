#include "TextFactory.h"

namespace Snk {

  Text *TextFactory::createText(const Texts type) {

    Text *text = NULL;

    switch (type) {
    case Texts::GameScreenLevel:
      text = new GameScreenLevel;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::GameScreenScore:
      text = new GameScreenScore;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::GameScreenLifes:
      text = new GameScreenLifes;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::InstructionScreenArrows:
      text = new InstructionsScreenArrows;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::InstructionScreenCredits:
      text = new InstructionsScreenCredits;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::InstructionScreenSpace:
      text = new InstructionsScreenSpace;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::InstructionScreenWarning:
      text = new InstructionsScreenWarning;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::PauseScreenMessage:
      text = new PauseScreenMessage;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::GameOverScreenMessage:
      text = new GameOverScreenMessage;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::GameOverScreenScore:
      text = new GameOverScreenScore;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::GameOverScreenLevel:
      text = new GameOverScreenLevel;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::GameOverScreenRestart:
      text = new GameOverScreenRestart;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::LevelUpScreenLetter:
      text = new LevelUpScreenLetter;
      text->loadFont(PATH_TEXT_FONT);
      break;

    case Texts::EndGameScreenMessage:
      text = new EndGameScreenMessage;
      text->loadFont(PATH_TEXT_FONT);
      break;

    default:
      std::cout << "Invalid text!" << std::endl;
      return NULL;
      break;
    }

    text->applyFont();
    text->setDefault();

    return text;
  }

} // namespace Snk
