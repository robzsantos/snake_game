#include "GameScreenLifes.h"

namespace Snk {

  GameScreenLifes::GameScreenLifes() {
    fontSize = 24;
    fillColor = sf::Color(252, 189, 0, 255);
    outlineColor = sf::Color::Black;
    outlineThickness = 2.0f;
    letterSpacing = 2.5f;
  }

  GameScreenLifes::~GameScreenLifes() { }

  void GameScreenLifes::loadFont(const std::string &path) {

    if (!font.loadFromFile(path))
      std::cout << "Error to load font: " << path << std::endl;
  }

  void GameScreenLifes::applyFont() { text.setFont(font); }

  void GameScreenLifes::setDefault() {
    text.setPosition(0, 0);
    text.setCharacterSize(fontSize);
    text.setFillColor(fillColor);
    text.setOutlineColor(outlineColor);
    text.setOutlineThickness(outlineThickness);
    text.setLetterSpacing(letterSpacing);
  }

  void GameScreenLifes::setPosition(const float x, const float y) {
    text.setPosition(x, y);
  }

  void GameScreenLifes::setFontSize(const int size) {
    text.setCharacterSize(size);
  }

  void GameScreenLifes::setFillColor(const sf::Color &color) {
    text.setFillColor(color);
  }

  void GameScreenLifes::setOutlineColor(const sf::Color &color) {
    text.setOutlineColor(color);
  }

  void GameScreenLifes::setLetterSpacing(const float spacing) {
    text.setLetterSpacing(spacing);
  }

  void GameScreenLifes::setOutlineThickness(const float thickness) {
    text.setOutlineThickness(thickness);
  }

  void GameScreenLifes::setText(const std::string &string) {
    text.setString(string);
  }

  sf::Text &GameScreenLifes::getDrawableText() { return text; }

} // namespace Snk
