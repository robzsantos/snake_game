#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "ScreenDimensions.h"
#include "text/TextFactory.h"

namespace Snk {

  class PauseScreen : public ScreenDimensions {

  public:
    PauseScreen(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset);

    ~PauseScreen();

    sf::RectangleShape &getBackground();

    sf::Text &getPauseText() const;

  private:
    sf::RectangleShape mBackground;

    Text *mPauseText = NULL;

    void m_drawBackground();

  };

}
