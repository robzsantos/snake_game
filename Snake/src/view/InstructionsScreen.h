#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "ScreenDimensions.h"
#include "sprite/SpriteFactory.h"
#include "text/TextFactory.h"

namespace Snk {

  class InstructionsScreen : public ScreenDimensions {

  public:
    InstructionsScreen(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset);

    ~InstructionsScreen();

    sf::RectangleShape &getBackground();

    sf::Sprite &getArrowsSprite() const;
    sf::Sprite &getSpaceSprite() const;

    sf::Text &getArrowsText() const;
    sf::Text &getSpaceText() const;
    sf::Text &getWarningText() const;
    sf::Text &getCreditsText() const;

  private:
    sf::RectangleShape mBackground;

    Sprite *mArrowsSprite = NULL;
    Sprite *mSpaceSprite = NULL;
    Sprite *mAppleSprite = NULL;

    Text *mArrowsText = NULL;
    Text *mSpaceText = NULL;
    Text *mWarningText = NULL;
    Text *mCreditsText = NULL;

    void m_drawBackground();

  };
} // namespace Snk
