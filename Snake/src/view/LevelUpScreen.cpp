#include "LevelUpScreen.h"

namespace Snk {

  LevelUpScreen::LevelUpScreen(const float wScreen, const float hScreen, 
    const float wOffset, const float hOffset)
      : ScreenDimensions(wScreen, hScreen, wOffset, hOffset) {

    mLetterL1 = TextFactory::createText(Texts::LevelUpScreenLetter);
    mLetterE1 = TextFactory::createText(Texts::LevelUpScreenLetter);
    mLetterV = TextFactory::createText(Texts::LevelUpScreenLetter);
    mLetterE2 = TextFactory::createText(Texts::LevelUpScreenLetter);
    mLetterL2 = TextFactory::createText(Texts::LevelUpScreenLetter);
    mLetterU = TextFactory::createText(Texts::LevelUpScreenLetter);
    mLetterP = TextFactory::createText(Texts::LevelUpScreenLetter);
    mExclamation = TextFactory::createText(Texts::LevelUpScreenLetter);

    mLetterL1->setText("L");
    mLetterE1->setText("E");
    mLetterV->setText("V");
    mLetterE2->setText("E");
    mLetterL2->setText("L");
    mLetterU->setText("U");
    mLetterP->setText("P");
    mExclamation->setText("!");

    mTimeAnimation = 7;
    mStartPosY = 30;
    reset();

    m_drawBackground();
  }

  LevelUpScreen::~LevelUpScreen() {
    if (mLetterL1) {
      delete mLetterL1;
      mLetterL1 = NULL;
    }
    if (mLetterL2) {
      delete mLetterL2;
      mLetterL2 = NULL;
    }
    if (mLetterE1) {
      delete mLetterE1;
      mLetterE1 = NULL;
    }
    if (mLetterE2) {
      delete mLetterE2;
      mLetterE2 = NULL;
    }
    if (mLetterV) {
      delete mLetterV;
      mLetterV = NULL;
    }
    if (mLetterU) {
      delete mLetterU;
      mLetterU = NULL;
    }
    if (mLetterP) {
      delete mLetterP;
      mLetterP = NULL;
    }
    if (mExclamation) {
      delete mExclamation;
      mExclamation = NULL;
    }
  }

  bool LevelUpScreen::isDoingAnimation() const { return doingAnimation; }

  sf::RectangleShape &LevelUpScreen::getBackground() { return mBackground; }

  sf::Text &LevelUpScreen::getSettedUpLetterL1() {

    const float posX = mLetterL1->getDrawableText().getPosition().x;
    const float posY = mLetterL1->getDrawableText().getPosition().y;

    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY * 2;

    if (posY > MIDDLE_SCREEN ||
        mLetterE1->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterL1->setPosition(posX, posY - mTimeAnimation);

    if (posY < 0) {
      doingAnimation = false;
    }

    return mLetterL1->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpLetterE1() const {

    const float posX = mLetterE1->getDrawableText().getPosition().x;
    const float posY = mLetterE1->getDrawableText().getPosition().y;

    const int tagPosYIn = static_cast<int>(mDimensions.y) - mStartPosY;
    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY * 2;

    if (mLetterL1->getDrawableText().getPosition().y < tagPosYIn &&
            posY > MIDDLE_SCREEN ||
        mLetterV->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterE1->setPosition(posX, posY - mTimeAnimation);

    return mLetterE1->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpLetterV() const {

    const float posX = mLetterV->getDrawableText().getPosition().x;
    const float posY = mLetterV->getDrawableText().getPosition().y;

    const int tagPosYIn = static_cast<int>(mDimensions.y) - mStartPosY * 2;
    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY * 2;

    if (mLetterE1->getDrawableText().getPosition().y < tagPosYIn &&
            posY > MIDDLE_SCREEN ||
        mLetterE2->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterV->setPosition(posX, posY - mTimeAnimation);

    return mLetterV->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpLetterE2() const {

    const float posX = mLetterE2->getDrawableText().getPosition().x;
    const float posY = mLetterE2->getDrawableText().getPosition().y;

    const int tagPosYIn = static_cast<int>(mDimensions.y) - mStartPosY * 3;
    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY * 2;

    if (mLetterV->getDrawableText().getPosition().y < tagPosYIn &&
            posY > MIDDLE_SCREEN ||
        mLetterL2->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterE2->setPosition(posX, posY - mTimeAnimation);

    return mLetterE2->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpLetterL2() const {

    const float posX = mLetterL2->getDrawableText().getPosition().x;
    const float posY = mLetterL2->getDrawableText().getPosition().y;

    const int tagPosYIn = static_cast<int>(mDimensions.y) - mStartPosY * 4;
    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY * 4;

    if (mLetterE2->getDrawableText().getPosition().y < tagPosYIn &&
            posY > MIDDLE_SCREEN ||
        mLetterU->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterL2->setPosition(posX, posY - mTimeAnimation);

    return mLetterL2->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpLetterU() const {

    const float posX = mLetterU->getDrawableText().getPosition().x;
    const float posY = mLetterU->getDrawableText().getPosition().y;

    const int tagPosYIn = static_cast<int>(mDimensions.y) - mStartPosY * 6;
    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY * 2;

    if (mLetterL2->getDrawableText().getPosition().y < tagPosYIn &&
            posY > MIDDLE_SCREEN ||
        mLetterP->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterU->setPosition(posX, posY - mTimeAnimation);

    return mLetterU->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpLetterP() const {

    const float posX = mLetterP->getDrawableText().getPosition().x;
    const float posY = mLetterP->getDrawableText().getPosition().y;

    const int tagPosYIn = static_cast<int>(mDimensions.y) - mStartPosY * 7;
    const int tagPosYOut = MIDDLE_SCREEN - mStartPosY;

    if (mLetterU->getDrawableText().getPosition().y < tagPosYIn &&
            posY > MIDDLE_SCREEN ||
        mExclamation->getDrawableText().getPosition().y <= tagPosYOut)
      mLetterP->setPosition(posX, posY - mTimeAnimation);

    return mLetterP->getDrawableText();
  }

  sf::Text &LevelUpScreen::getSettedUpExclamation() {

    const float posX = mExclamation->getDrawableText().getPosition().x;
    const float posY = mExclamation->getDrawableText().getPosition().y;

    const int tagPosY = static_cast<int>(mDimensions.y) - mStartPosY * 8;

    if (mLetterP->getDrawableText().getPosition().y < tagPosY &&
            posY > MIDDLE_SCREEN ||
        mCount == SECOND)
      mExclamation->setPosition(posX, posY - mTimeAnimation);
    else if (posY == MIDDLE_SCREEN)
      ++mCount;

    return mExclamation->getDrawableText();
  }

  void LevelUpScreen::reset() {

    mLetterL1->setPosition(290.f, mDimensions.y);
    mLetterE1->setPosition(310.f, mDimensions.y);
    mLetterV->setPosition(330.f, mDimensions.y);
    mLetterE2->setPosition(350.f, mDimensions.y);
    mLetterL2->setPosition(370.f, mDimensions.y);
    mLetterU->setPosition(410.f, mDimensions.y);
    mLetterP->setPosition(435.f, mDimensions.y);
    mExclamation->setPosition(455.f, mDimensions.y);

    mCount = 0;
    doingAnimation = true;
  }

  void LevelUpScreen::m_drawBackground() {
    mBackground.setSize(sf::Vector2f(mDimensions.x, 100.f));
    mBackground.setPosition(sf::Vector2f(0.f, 250.f));
    mBackground.setFillColor(BLACK_35PERCENT_TRANSPARENT);
  }

} // namespace Snk
