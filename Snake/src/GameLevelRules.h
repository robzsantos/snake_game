#pragma once
#include "Enums.h"
#include <array>

namespace Snk {

  static const int GAME_MAX_LEVEL = 22;
  static const std::array<int, GAME_MAX_LEVEL> GAME_SPEED_LEVEL = {
      8, 8, 7, 7, 7, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 2};
  static const std::array<int, GAME_MAX_LEVEL> GAME_BODY_LENGTH_LEVEL = {
      10, 13, 13, 13, 18, 18, 18, 18, 23, 23, 23,
      28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28};
  static const std::array<TerrainSize, GAME_MAX_LEVEL> GAME_TERRAIN_SIZE_LEVEL = {
      TerrainSize::T1, TerrainSize::T1, TerrainSize::T1, TerrainSize::T2,
      TerrainSize::T3, TerrainSize::T2, TerrainSize::T3, TerrainSize::T4,
      TerrainSize::T1, TerrainSize::T2, TerrainSize::T3, TerrainSize::T1,
      TerrainSize::T2, TerrainSize::T3, TerrainSize::T1, TerrainSize::T2,
      TerrainSize::T3, TerrainSize::T4, TerrainSize::T1, TerrainSize::T2,
      TerrainSize::T3, TerrainSize::T4};

} // namespace Snk
