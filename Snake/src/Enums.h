#pragma once

namespace Snk {

  enum class States {
    StartGame,
    SnakeMoving,
    SnakeEatingApple,
    SnakeDying,
    SnakeRespawn,
    PauseGame,
    LevelUp,
    GameOver,
    EndGame
  };

  enum class TerrainSize { T1, T2, T3, T4 };

  enum class LotOccupation { Free, Apple, Snake };

  enum class SnakeDirection { Down = 0, Left, Up, Right };

  enum class SnakePart { Head, Body, Tail };

  enum class SoundEffects { SnakeMove, SnakeEat, SnakeHit, LevelUp, GameOver };

  enum class Sprites { BackgroundGame, Apple, Snake, ArrowsKb, SpaceKb };

  enum class Texts {
    GameScreenLevel,
    GameScreenScore,
    GameScreenLifes,
    InstructionScreenArrows,
    InstructionScreenSpace,
    InstructionScreenWarning,
    InstructionScreenCredits,
    PauseScreenMessage,
    GameOverScreenMessage,
    GameOverScreenScore,
    GameOverScreenLevel,
    GameOverScreenRestart,
    LevelUpScreenLetter,
    EndGameScreenMessage
  };

} // namespace Snk