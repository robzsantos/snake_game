#include "GameView.h"

namespace Snk {
  GameView::GameView(const float wScreen, const float hScreen, const float wOffset, 
    const float hOffset, const std::string &title) {

    mGameWindow.create(
        sf::VideoMode(static_cast<int>(wScreen), static_cast<int>(hScreen)),
        title);
    mGameWindow.setFramerateLimit(60);

    mGameScreen =
        std::make_unique<GameScreen>(wScreen, hScreen, wOffset, hOffset);
    mInstructionsScreen =
        std::make_unique<InstructionsScreen>(wScreen, hScreen, wOffset, hOffset);
    mPauseScreen =
        std::make_unique<PauseScreen>(wScreen, hScreen, wOffset, hOffset);
    mLevelUpScreen =
        std::make_unique<LevelUpScreen>(wScreen, hScreen, wOffset, hOffset);
    mGameOverScreen =
        std::make_unique<GameOverScreen>(wScreen, hScreen, wOffset, hOffset);
    mEndGameScreen =
        std::make_unique<EndGameScreen>(wScreen, hScreen, wOffset, hOffset);

    mGameSounds = std::make_unique<GameSounds>();
  }

  GameView::~GameView() {}

  sf::RenderWindow &GameView::getGameWindow() { return mGameWindow; }

  bool GameView::windowIsOpen() const { return mGameWindow.isOpen(); }

  void GameView::close() { mGameWindow.close(); }

  void GameView::refresh() { mGameWindow.display(); }

  void GameView::paintGameScreen() {
    mGameWindow.draw(mGameScreen->getBgGameSprite());

    mGameWindow.draw(mGameScreen->getHudBg());

    mGameWindow.draw(mGameScreen->getAppleHudSprite());

    mGameWindow.draw(mGameScreen->getLevelText());

    mGameWindow.draw(mGameScreen->getScoreText());

    mGameWindow.draw(mGameScreen->getLifesText());
  }

  // TEMP
  void GameView::paintPiece(Lot &piece) {

    mGameWindow.draw(mGameScreen->getSettedUpPiece(piece));
  }

  void GameView::paintTerrain(const float offsetX, const float offsetY) {
    mGameWindow.draw(mGameScreen->getSettedUpTerrain(offsetX, offsetY));
  }

  void GameView::paintSnake(Snake *snake) {

    mGameWindow.draw(mGameScreen->getSettedUpSnakeHead(snake));
    mGameWindow.draw(mGameScreen->getSettedUpSnakeTail(snake));

    for (auto &body : snake->getBody()) {
      mGameWindow.draw(mGameScreen->getSettedUpSnakeBody(body.get()));
    }
  }

  void GameView::paintApple(Apple *apple) {

    mGameWindow.draw(mGameScreen->getSettedUpApple(apple));
  }

  void GameView::paintInstructionsScreen() {

    mGameWindow.draw(mInstructionsScreen->getBackground());

    mGameWindow.draw(mInstructionsScreen->getSpaceSprite());

    mGameWindow.draw(mInstructionsScreen->getArrowsSprite());

    mGameWindow.draw(mInstructionsScreen->getArrowsText());

    mGameWindow.draw(mInstructionsScreen->getSpaceText());

    mGameWindow.draw(mInstructionsScreen->getWarningText());

    mGameWindow.draw(mInstructionsScreen->getCreditsText());
  }

  void GameView::paintPauseScreen() {

    mGameWindow.draw(mPauseScreen->getBackground());

    mGameWindow.draw(mPauseScreen->getPauseText());
  }

  void GameView::paintLevelUpScreen() {

    mGameWindow.draw(mLevelUpScreen->getBackground());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterL1());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterE1());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterV());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterE2());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterL2());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterU());

    mGameWindow.draw(mLevelUpScreen->getSettedUpLetterP());

    mGameWindow.draw(mLevelUpScreen->getSettedUpExclamation());
  }

  void GameView::paintGameOverScreen(const int score, const int maxLevel) {

    mGameWindow.draw(mGameOverScreen->getBackground());

    mGameWindow.draw(mGameOverScreen->getGameOverText());

    mGameWindow.draw(mGameOverScreen->getSettedUpScoreText(score));

    mGameWindow.draw(mGameOverScreen->getSettedUpLevelText(maxLevel));

    mGameWindow.draw(mGameOverScreen->getRestartText());
  }

  void GameView::paintEndGameScreen() {

    mGameWindow.draw(mEndGameScreen->getBackground());

    mGameWindow.draw(mEndGameScreen->getEndGameMessageText());

    mGameWindow.draw(mEndGameScreen->getRestartText());
  }

  void GameView::updateScore(const int score) { mGameScreen->updateScore(score); }

  void GameView::updateLevel(const int level) { mGameScreen->updateLevel(level); }

  void GameView::updateLifes(const int lifes) { mGameScreen->updateLifes(lifes); }

  void GameView::playSfx(const SoundEffects type) {
    switch (type) {
    case SoundEffects::SnakeMove:
      mGameSounds->playSnakeMoveSound();
      break;
    case SoundEffects::SnakeEat:
      mGameSounds->playSnakeEatSound();
      break;
    case SoundEffects::SnakeHit:
      mGameSounds->stopSnakeMoveSound();
      mGameSounds->playSnakeHitSound();
      break;
    case SoundEffects::LevelUp:
      mGameSounds->playLevelUpSound();
      break;
    case SoundEffects::GameOver:
      mGameSounds->playGameOverSound();
      break;

    default:
      break;
    }
  }

  void GameView::resetLevelUpScreen() { mLevelUpScreen->reset(); }

  bool GameView::isDoingAnimation() const { return mLevelUpScreen->isDoingAnimation(); }
} // namespace Snk
