#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>
#include <string>

namespace Snk {

  static const sf::Vector2f WINDOW_DIMENSIONS{755.f, 600.f};
  static const sf::Vector2f WINDOW_OFFSET{21.f, 52.f};

  static const sf::Color BLACK_35PERCENT_TRANSPARENT(0, 0, 0, 167);
  static const sf::Color BLACK_50PERCENT_TRANSPARENT(0, 0, 0, 127);

  static const sf::Vector2f INSTRUCTIONS_SPRITE_ARROW_POSITION{140.f, 100.f};
  static const sf::Vector2f INSTRUCTIONS_SPRITE_SPACE_POSITION{420.f, 145.f};
  static const sf::Vector2f INSTRUCTIONS_TEXT_ARROW_POSITION{48.f, 200.f};
  static const sf::Vector2f INSTRUCTIONS_TEXT_SPACE_POSITION{416.f, 200.f};
  static const sf::Vector2f INSTRUCTIONS_TEXT_MSG_POSITION{98.f, 325.f};
  static const sf::Vector2f INSTRUCTIONS_TEXT_CREDITS_POSITION{8.f, 530.f};

  static const sf::Vector2f PAUSE_TEXT_MSG_POSITION{200.f, 240.f};

  static const sf::Vector2f GAME_SPRITE_APPLE_HUD_POSITION{8.f, 5.f};
  static const sf::Vector2f GAME_TEXT_LIFE_POSITION{325.f, 10.f};
  static const sf::Vector2f GAME_TEXT_LEVEL_POSITION{620.f, 10.f};
  static const sf::Vector2f GAME_TEXT_SCORE_POSITION{50.f, 10.f};

  static const sf::Vector2f GAMEOVER_TEXT_MSG_POSITION{230.f, 220.f};
  static const sf::Vector2f GAMEOVER_TEXT_LEVEL_POSITION{285.f, 375.f};
  static const sf::Vector2f GAMEOVER_TEXT_SCORE_POSITION{200.f, 335.f};
  static const sf::Vector2f GAMEOVER_TEXT_RESTART_POSITION{250.f, 430.f};

  static const sf::Vector2f ENDGAME_TEXT_MSG_POSITION{0.f, 200.f};
  static const sf::Vector2f ENDGAME_TEXT_RESTART_POSITION{250.f, 460.f};

  //##---TEXTS---##

  static const std::string TEXT_WINDOW_TITLE = "Snake";
  static const std::string TEXT_PAUSE_GAME =
      "\t\t\t Pause Game\n\nPress SPACE to continue";

  static const std::string TEXT_INSTRUCTIONS_ARROW =
      "Use the ARROWS to change\n\tthe direction of Snake";
  static const std::string TEXT_INSTRUCTIONS_SPACE =
      "\t\tPress SPACE to\n  pause/continue game";
  static const std::string TEXT_INSTRUCTIONS_MSG =
      "\t Make the Snake eat the Apples\nBut be careful to not get "
      "stuck!\n\n\t\t\t\t\tClick to Start";
  static const std::string TEXT_INSTRUCTIONS_CREDITS =
      "Snake Art by Kenney\nBackground Grass Art by Kiira\n\"Power Up, Bright, "
      "A.wav\" by InspectorJ (www.jshaw.co.uk) of\nFreesound.org";

  static const std::string TEXT_GAME_SCORE = "Eaten: ";
  static const std::string TEXT_GAME_LEVEL = "Level: ";
  static const std::string TEXT_GAME_LIFE = "Life: 0";

  static const std::string TEXT_GAMEOVER_MSG = "GAME OVER!";
  static const std::string TEXT_GAMEOVER_RESTART = "Click to Restart";
  static const std::string TEXT_GAMEOVER_SCORE = "Total Apples Eaten: ";
  static const std::string TEXT_GAMEOVER_LEVEL = "Max Level: ";

  static const std::string TEXT_ENDGAME_MSG =
      "\t\t\tCONGRATULATIONS!\n\n\t  You won the last level\n\t\t\t   of the "
      "game! :-)";

  //##---ASSETS---##

  static const std::string PATH_TEXT_FONT = "font/showcard_gothic.ttf";

  static const std::string PATH_IMG_BACKGROUND = "images/background.png";
  static const std::string PATH_IMG_APPLE = "images/apple.png";
  static const std::string PATH_IMG_SNAKE = "images/snake_sprite.png";
  static const std::string PATH_IMG_ARROW_KEYS = "images/arrow_keys.png";
  static const std::string PATH_IMG_SPACE_KEY = "images/space_key.png";

  static const std::string PATH_SND_SNAKE_MOVING = "sounds/move.flac";
  static const std::string PATH_SND_SNAKE_EATING = "sounds/eat.flac";
  static const std::string PATH_SND_SNAKE_DIYNG = "sounds/death.flac";
  static const std::string PATH_SND_LEVEL_UP = "sounds/level_up.flac";
  static const std::string PATH_SND_GAME_OVER = "sounds/game_over.flac";

  //##---APPLE---##

  static const float GAME_APPLE_SIZE = 35.f;

  //##---SNAKE---##

  static const int GAME_SPRITE_SNAKE_TAIL_FRAME_POSITION = 5;
  static const int GAME_SPRITE_SNAKE_BODY_FRAME_POSITION = 4;
  static const int GAME_SNAKE_START_INDENT_COL_POSITION = 5;
  static const int GAME_SNAKE_START_INDENT_ROW_POSITION = 1;
  static const int GAME_SNAKE_BODY_PART_SIZE = 42;
  static const int GAME_SNAKE_START_LIFE = 3;
  static const int GAME_SNAKE_INIT_LENGTH = 3;

  //##---TERRAIN---##

  static const sf::Vector2i GAME_TERRAIN_T2_INDENT = {2, 2};
  static const sf::Vector2i GAME_TERRAIN_T3_INDENT = {4, 2};
  static const sf::Vector2i GAME_TERRAIN_T4_INDENT = {6, 4};
  static const sf::Vector2f GAME_TERRAIN_DIMENSIONS = {755.f, 550.f};
  static const float GAME_TERRAIN_LOT_SIZE = 42.f;

} // namespace Snk