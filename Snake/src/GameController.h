#pragma once

#include "GameModel.h"
#include "GameView.h"
#include "input/InputEventsPublisher.h"
#include "input/InputListener.h"

namespace Snk {

  class GameController : public InputListener {
  
  public:
    GameController(GameView &view, GameModel &model);

    ~GameController();

    void processEventsQueue();

    void reset();

    //TODO:: think about maintain this methods public
    void onMouseDown(const sf::Event &event);

    void onMouseUp(const sf::Event &event);

    void onKeyDown(const sf::Event &event);

    void onKeyUp(const sf::Event &event);

    void onClose();

    void onFocusOut();

    void onFocusIn();

  private:
    GameView &mView;

    GameModel &mModel;

    InputEventsPublisher mEventsPub;

    sf::Event::KeyEvent mLastKeyPressed;

    bool mInducePaused;

  };

} // namespace Snk
