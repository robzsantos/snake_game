#include "../GameModel.h"
#include "StatePauseGame.h"

namespace Snk {

  StatePauseGame::StatePauseGame(GameModel *context) : State(context) {}

  StatePauseGame::~StatePauseGame() {}

  void StatePauseGame::enter() {}

  void StatePauseGame::update() {
    if (mContext->getPreviousState() == States::LevelUp) {
      mContext->getView().paintLevelUpScreen();
    }
    mContext->getView().paintPauseScreen();
  }

  void StatePauseGame::exit() {}

}
