#pragma once

#include "State.h"

namespace Snk {

  class StateSnakeRespawn : public State {

  public:
    StateSnakeRespawn(GameModel *context);

    ~StateSnakeRespawn();

    void enter();

    void update();

    void exit();
  };
}
