#pragma once

#include "State.h"

namespace Snk {

  class StateGameOver : public State {
 
  public:
    StateGameOver(GameModel *context);

    ~StateGameOver();

    void enter();

    void update();

    void exit();
  };

}