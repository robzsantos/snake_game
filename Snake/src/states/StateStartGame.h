#pragma once

#include "State.h"

namespace Snk {

  class StateStartGame : public State {

  public:
    StateStartGame(GameModel *context);

    ~StateStartGame();

    void enter();

    void update();

    void exit();
  };

}