#include "../GameModel.h"
#include "StateGameOver.h"

namespace Snk {

  StateGameOver::StateGameOver(GameModel *context) : State(context) {}

  StateGameOver::~StateGameOver() {}

  void StateGameOver::enter() {
    // TODO::change properties of screen here ;)
    mContext->getView().playSfx(SoundEffects::GameOver);
  }

  void StateGameOver::update() { mContext->setGameOver(); }

  void StateGameOver::exit() {}

}