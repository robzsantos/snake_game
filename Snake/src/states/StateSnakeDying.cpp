#include "../GameModel.h"
#include "StateSnakeDying.h"

namespace Snk {

  StateSnakeDying::StateSnakeDying(GameModel *context) : State(context) {}

  StateSnakeDying::~StateSnakeDying() {}

  void StateSnakeDying::enter() {}

  void StateSnakeDying::update() {
    Snake *snake = mContext->getSnake();
    Terrain *terrain = mContext->getTerrain();

    snake->decreaseLife();
    mContext->getView().playSfx(SoundEffects::SnakeHit);
    mContext->getView().updateLifes(snake->getLifes());

    terrain->cleanLot(snake->getHead()->getPrevRow(),
                      snake->getHead()->getPrevColumn());
    terrain->cleanLot(snake->getTail()->getPrevRow(),
                      snake->getTail()->getPrevColumn());

    for (auto &body : snake->getBody()) {
      terrain->cleanLot(body->getPrevRow(), body->getPrevColumn());
    }

    if (snake->getLifes() > 0) {
      mContext->setNextState(States::SnakeRespawn);
    } else {
      mContext->setNextState(States::GameOver);
    }
  }

  void StateSnakeDying::exit() {}

} // namespace Snk
