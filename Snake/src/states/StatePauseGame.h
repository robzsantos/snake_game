#pragma once

#include "State.h"

namespace Snk {

  class StatePauseGame : public State {

  public:
    StatePauseGame(GameModel *context);

    ~StatePauseGame();

    void enter();

    void update();

    void exit();
  };

}