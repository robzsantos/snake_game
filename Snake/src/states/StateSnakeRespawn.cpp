#include "../GameModel.h"
#include "StateSnakeRespawn.h"

namespace Snk {

  StateSnakeRespawn::StateSnakeRespawn(GameModel *context) : State(context) {}

  StateSnakeRespawn::~StateSnakeRespawn() {}

  void StateSnakeRespawn::enter() {
    Snake *snake = mContext->getSnake();
    Terrain *terrain = mContext->getTerrain();

    snake->resetSnake(
        terrain->getNumLotsY() - GAME_SNAKE_START_INDENT_ROW_POSITION,
        terrain->getNumLotsX() - GAME_SNAKE_START_INDENT_COL_POSITION);

    // TODO::improve readability of this `if`
    if (mContext->getPreviousState() == States::LevelUp ||
        terrain->getLots()[snake->getHead()->getRow()]
                          [snake->getHead()->getColumn()]
                              .getOccupation() == LotOccupation::Apple ||
        terrain->getLots()[snake->getTail()->getRow()]
                          [snake->getTail()->getColumn()]
                              .getOccupation() == LotOccupation::Apple ||
        terrain->getLots()[snake->getBody()[0]->getRow()]
                          [snake->getBody()[0]->getColumn()]
                              .getOccupation() == LotOccupation::Apple) {

      snake->setNextPositionByLot(terrain);
      mContext->createApple();
    } else {

      snake->setNextPositionByLot(terrain);
    }
  }

  void StateSnakeRespawn::update() {
    mContext->setNextState(States::SnakeMoving);
  }

  void StateSnakeRespawn::exit() {}

} // namespace Snk
