#pragma once

#include "State.h"

namespace Snk {

  class StateSnakeDying : public State {
 
  public:
    StateSnakeDying(GameModel *context);

    ~StateSnakeDying();

    void enter();

    void update();

    void exit();
  };

}