#include "../GameModel.h"
#include "StateStartGame.h"

namespace Snk {

  StateStartGame::StateStartGame(GameModel *context) : State(context) {}

  StateStartGame::~StateStartGame() {}

  void StateStartGame::enter() {}

  void StateStartGame::update() { mContext->getView().paintInstructionsScreen(); }

  void StateStartGame::exit() {}

}
