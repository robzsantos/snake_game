#include "../GameModel.h"
#include "StateLevelUp.h"

namespace Snk {

  StateLevelUp::StateLevelUp(GameModel *context) : State(context) {}

  StateLevelUp::~StateLevelUp() {}

  void StateLevelUp::enter() {
    if (mContext->getPreviousState() != States::PauseGame) {
      mContext->getView().playSfx(SoundEffects::LevelUp);
    }
  }

  void StateLevelUp::update() {
    Snake *snake = mContext->getSnake();

    snake->fadeOut();
    if (mContext->getView().isDoingAnimation()) {
      mContext->getView().paintLevelUpScreen();
    } else {
      mContext->getView().resetLevelUpScreen();

      if (mContext->getLevel() < GAME_MAX_LEVEL) {
        mContext->increaseLevel();
        mContext->increaseSpeed();
        mContext->resetTerrain();

        mContext->setNextState(States::SnakeRespawn);
      } else {
        mContext->setNextState(States::EndGame);
      }
    }
  }

  void StateLevelUp::exit() {}

} // namespace Snk
