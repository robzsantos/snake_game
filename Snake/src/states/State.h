#pragma once

namespace Snk {

  class GameModel;

  class State {

  public:
    State(GameModel *context);
    virtual ~State();

    virtual void enter() = 0;

    virtual void update() = 0;

    virtual void exit() = 0;

  protected:
    GameModel *mContext;
  };

}
