#include "../GameModel.h"
#include "StateEndGame.h"

namespace Snk {

  StateEndGame::StateEndGame(GameModel *context) : State(context) {}

  StateEndGame::~StateEndGame() {}

  void StateEndGame::enter() {
    // TODO::change properties of screen here ;)
  }

  void StateEndGame::update() { mContext->getView().paintEndGameScreen(); }

  void StateEndGame::exit() {}

}