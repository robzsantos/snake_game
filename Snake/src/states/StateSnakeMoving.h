#pragma once

#include "State.h"

namespace Snk {

  class StateSnakeMoving : public State {

  public:
    StateSnakeMoving(GameModel *context);

    ~StateSnakeMoving();

    void enter();

    void update();

    void exit();
  };

}