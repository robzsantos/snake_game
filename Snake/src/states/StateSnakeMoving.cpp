#include "../GameModel.h"
#include "StateSnakeMoving.h"

namespace Snk {

  StateSnakeMoving::StateSnakeMoving(GameModel *context) : State(context) {}

  StateSnakeMoving::~StateSnakeMoving() {}

  void StateSnakeMoving::enter() {}

  void StateSnakeMoving::update() {
    Snake *snake = mContext->getSnake();
    Terrain *terrain = mContext->getTerrain();

    if (mContext->canMove()) {

      snake->move();
      mContext->getView().playSfx(SoundEffects::SnakeMove);

      if (snake->getHead()->getRow() < 0 ||
          snake->getHead()->getRow() >= terrain->getNumLotsY() ||
          snake->getHead()->getColumn() < 0 ||
          snake->getHead()->getColumn() >=
              terrain->getNumLotsX()) { // check terrain boundaries

        mContext->setNextState(States::SnakeDying);
        return;
      }

      const LotOccupation occupation =
          terrain
              ->getLots()[snake->getHead()->getRow()]
                         [snake->getHead()->getColumn()]
              .getOccupation();

      switch (occupation) {
      case LotOccupation::Free:
        snake->setNextPositionByLot(terrain);

        terrain->cleanLot(snake->getTail()->getPrevRow(),
                          snake->getTail()->getPrevColumn());
        break;

      case LotOccupation::Apple:
        mContext->setNextState(States::SnakeEatingApple);
        break;

      case LotOccupation::Snake:
        mContext->setNextState(States::SnakeDying);
        break;
      }
    }
  }

  void StateSnakeMoving::exit() {}

} // namespace Snk
