#pragma once

#include "State.h"

namespace Snk {
  class StateEndGame : public State {
 
  public:
    StateEndGame(GameModel *context);

    ~StateEndGame();

    void enter();

    void update();

    void exit();
  };
}
