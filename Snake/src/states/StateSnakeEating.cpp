#include "../GameModel.h"
#include "StateSnakeEating.h"

namespace Snk {

  StateSnakeEating::StateSnakeEating(GameModel *context) : State(context) {}

  StateSnakeEating::~StateSnakeEating() {}

  void StateSnakeEating::enter() {}

  void StateSnakeEating::update() {
    Snake *snake = mContext->getSnake();
    Terrain *terrain = mContext->getTerrain();

    mContext->increaseApples();
    mContext->getView().playSfx(SoundEffects::SnakeEat);

    snake->increaseBody();
    snake->setNextPositionByLot(terrain);

    if (snake->getBodyLength() == mContext->getCurrentBodyLevel()) {
      mContext->resetApple();
      mContext->setNextState(States::LevelUp);
    } else {
      mContext->createApple();
      mContext->setNextState(States::SnakeMoving);
    }
  }

  void StateSnakeEating::exit() {}

} // namespace Snk
