#pragma once

#include "State.h"

namespace Snk{

  class StateLevelUp : public State {

  public:
    StateLevelUp(GameModel *context);

    ~StateLevelUp();

    void enter();

    void update();

    void exit();
  };
}