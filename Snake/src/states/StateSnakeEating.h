#pragma once

#include "State.h"

namespace Snk {

  class StateSnakeEating : public State {
 
  public:
    StateSnakeEating(GameModel *context);

    ~StateSnakeEating();

    void enter();

    void update();

    void exit();
  };

}