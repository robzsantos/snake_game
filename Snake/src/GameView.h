#pragma once
#include <SFML/Graphics.hpp>

#include "view/EndGameScreen.h"
#include "view/GameOverScreen.h"
#include "view/GameScreen.h"
#include "view/InstructionsScreen.h"
#include "view/LevelUpScreen.h"
#include "view/PauseScreen.h"
#include "view/sound/GameSounds.h"

namespace Snk {
  class GameView {

  public:
    GameView(const float wScreen, const float hScreen, const float wOffset, 
      const float hOffset, const std::string &title);

    ~GameView();

    sf::RenderWindow &getGameWindow();

    bool windowIsOpen() const;

    void close();

    void refresh();

    void paintGameScreen();

    // TEMP
    void paintPiece(Lot &piece);

    void paintTerrain(const float offsetX, const float offsetY);

    void paintSnake(Snake *snake);

    void paintApple(Apple *apple);

    void paintInstructionsScreen();

    void paintPauseScreen();

    void paintLevelUpScreen();

    void paintGameOverScreen(const int score, const int maxLevel);

    void paintEndGameScreen();

    void updateScore(const int score);

    void updateLevel(const int level);

    void updateLifes(const int lifes);

    void playSfx(const SoundEffects type);

    void resetLevelUpScreen();

    bool isDoingAnimation() const;

  private:
    sf::RenderWindow mGameWindow;

    std::unique_ptr<GameScreen> mGameScreen{nullptr};
    std::unique_ptr<InstructionsScreen> mInstructionsScreen{nullptr};
    std::unique_ptr<PauseScreen> mPauseScreen{nullptr};
    std::unique_ptr<LevelUpScreen> mLevelUpScreen{nullptr};
    std::unique_ptr<GameOverScreen> mGameOverScreen{nullptr};
    std::unique_ptr<EndGameScreen> mEndGameScreen{nullptr};
    std::unique_ptr<GameSounds> mGameSounds{nullptr};
  };
} // namespace Snk
