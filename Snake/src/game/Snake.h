#pragma once

#include <vector>

#include "SnakeBody.h"
#include "SnakeHead.h"
#include "SnakeTail.h"
#include "Terrain.h"

namespace Snk {

  class Snake {

  public:
    Snake(const int startRow, const int startCol);

    ~Snake();

    void decreaseLife();

    void setSpeed(const int value);

    int getLifes() const;

    int getSpeed() const;

    bool isWaitingMove() const;

    void resetSnake(const int startRow, const int startCol);

    void setNextPositionByLot(Terrain *terrain);

    void setPosition(const float posX, const float posY, const SnakePart part,
      const int currentPos);

    void changeDirection(const SnakeDirection direction);

    void move();

    void increaseBody();

    bool fadeOut() const;

    std::vector<std::unique_ptr<SnakeBody>> &getBody();
    std::unique_ptr<SnakeHead> &getHead();
    std::unique_ptr<SnakeTail> &getTail();
    int getBodyLength() const;

  private:
    int mSpeed;
    int mBodyLength;
    int mLifes;
    bool mWaitingMove;

    std::unique_ptr<SnakeHead> mHead{nullptr};
    std::vector<std::unique_ptr<SnakeBody>> mBody;
    std::unique_ptr<SnakeTail> mTail{nullptr};

    std::unique_ptr<SnakeBody> m_createBody(const int row, const int column,
      const SnakeDirection direction) const;
  };

} // namespace Snk
