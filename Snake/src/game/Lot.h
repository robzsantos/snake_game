#pragma once

#include "../Enums.h"

namespace Snk {

  class Lot {

  public:
    Lot(const float posX, const float posY, const float size, const int row,
      const int col);

    ~Lot();

    int getRow() const;
    int getColumn() const;
    float getPosX() const;
    float getPosY() const;
    float getSize() const;
    LotOccupation getOccupation() const;

    void setOccupation(const LotOccupation value);

  private:
    int mRow;
    int mColumn;
    float mPosX;
    float mPosY;
    float mSize;
    LotOccupation mOccupation;
  };

}
