#include "SnakeHead.h"

namespace Snk {

  SnakeHead::SnakeHead(const int row, const int column, const int size, 
    const SnakeDirection direction)
    : SnakeBodyParts(row, column, size, direction) {}

  SnakeHead::~SnakeHead() {}

  void SnakeHead::move(SnakeBodyParts *previous) {
    mPrevCol = mCurrentCol;
    mPrevRow = mCurrentRow;

    switch (mCurrentDirection) {
    case SnakeDirection::Left:
      --mCurrentCol;
      break;
    case SnakeDirection::Up:
      --mCurrentRow;
      break;
    case SnakeDirection::Right:
      ++mCurrentCol;
      break;
    case SnakeDirection::Down:
      ++mCurrentRow;
      break;
    }

    mPrevDirection = mCurrentDirection;
  }

  void SnakeHead::changeDirection(const SnakeDirection direction) {
    mCurrentDirection = direction;
  }

} // namespace Snk
