#include "Terrain.h"

namespace Snk {

  Terrain::Terrain(const float width, const float height, const float lotSize,
          const TerrainSize tSize)
      : mTerrainTotalSize(width, height), mLotSize(lotSize) {

    createTerrain(tSize);
  }

  Terrain::~Terrain() {}

  std::vector<std::vector<Lot>> &Terrain::getLots() { return mLots; }

  int Terrain::getNumLotsX() const { return numLotsX; }

  int Terrain::getNumLotsY() const { return numLotsY; }

  float Terrain::getOffsetX() const { return mOffsetX; }

  float Terrain::getOffsetY() const { return mOffsetY; }

  Lot &Terrain::getLotFree() {

    int column = -1;
    int row = -1;

    do {
      row = rand() % numLotsY;
      column = rand() % numLotsX;
    } while (mLots[row][column].getOccupation() != LotOccupation::Free);

    return mLots[row][column];
  }

  void Terrain::cleanLot(const int row, const int column) {
    mLots[row][column].setOccupation(LotOccupation::Free);
  }

  void Terrain::createTerrain(const TerrainSize tSize) {

    sf::Vector2f terrainAvailableSize;

    mLots.clear();

    switch (tSize) {
    case TerrainSize::T1:
      terrainAvailableSize.x = mTerrainTotalSize.x;
      terrainAvailableSize.y = mTerrainTotalSize.y;

      numLotsX = static_cast<int>(mTerrainTotalSize.x / mLotSize);
      numLotsY = static_cast<int>(mTerrainTotalSize.y / mLotSize);

      break;
    case TerrainSize::T2:
      terrainAvailableSize.x =
          mTerrainTotalSize.x - GAME_TERRAIN_T2_INDENT.x * mLotSize;
      terrainAvailableSize.y =
          mTerrainTotalSize.y - GAME_TERRAIN_T2_INDENT.y * mLotSize;

      numLotsX = static_cast<int>(terrainAvailableSize.x / mLotSize);
      numLotsY = static_cast<int>(terrainAvailableSize.y / mLotSize);

      break;
    case TerrainSize::T3:
      terrainAvailableSize.x =
          mTerrainTotalSize.x - GAME_TERRAIN_T3_INDENT.x * mLotSize;
      terrainAvailableSize.y =
          mTerrainTotalSize.y - GAME_TERRAIN_T3_INDENT.y * mLotSize;

      numLotsX = static_cast<int>(terrainAvailableSize.x / mLotSize);
      numLotsY = static_cast<int>(terrainAvailableSize.y / mLotSize);

      break;
    case TerrainSize::T4:
      terrainAvailableSize.x =
          mTerrainTotalSize.x - GAME_TERRAIN_T4_INDENT.x * mLotSize;
      terrainAvailableSize.y =
          mTerrainTotalSize.y - GAME_TERRAIN_T4_INDENT.y * mLotSize;

      numLotsX = static_cast<int>(terrainAvailableSize.x / mLotSize);
      numLotsY = static_cast<int>(terrainAvailableSize.y / mLotSize);

      break;

    default:
      break;
    }

    mOffsetX = (mTerrainTotalSize.x - terrainAvailableSize.x) / 2;
    mOffsetY = (mTerrainTotalSize.y - terrainAvailableSize.y) / 2;

    for (int i = 0; i < numLotsY; i++) {
      std::vector<Lot> row;
      for (int j = 0; j < numLotsX; j++) {
        Lot lot =
            Lot(mOffsetX + j * mLotSize, mOffsetY + i * mLotSize, mLotSize, i, j);
        row.push_back(lot);
      }
      mLots.push_back(row);
    }
  }

} // namespace Snk
