#pragma once

#include "SnakeBodyParts.h"

namespace Snk {

  class SnakeBody : public SnakeBodyParts {

  public:
    SnakeBody(const int row, const int column, const int size, 
      const SnakeDirection direction);

    ~SnakeBody();

    void move(SnakeBodyParts *previous = nullptr);

    void setPreviousCoordinates();

  };

}
