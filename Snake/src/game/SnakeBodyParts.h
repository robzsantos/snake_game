#pragma once

#include "../Enums.h"

namespace Snk {

  class SnakeBodyParts {

  public:
    SnakeBodyParts(const int row, const int column, const int size, 
      const SnakeDirection direction);

    ~SnakeBodyParts();

    int getRow() const;
    int getColumn() const;
    int getPrevRow() const;
    int getPrevColumn() const;
    int getAlpha() const;
    int getSize() const;
    float getPosX() const;
    float getPosY() const;
    SnakeDirection getDirection() const;
    SnakeDirection getPrevDirection() const;

    void setPosition(const float posX, const float posY);

    void fadeOut(const int decreaseValue);

    virtual void move(SnakeBodyParts *previous = nullptr) = 0;

  protected:
    int mSize;
    int mAlpha;
    int mCurrentRow;
    int mCurrentCol;
    int mPrevRow;
    int mPrevCol;
    float mPosX;
    float mPosY;
    SnakeDirection mCurrentDirection;
    SnakeDirection mPrevDirection;
  };

}

