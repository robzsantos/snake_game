#pragma once

namespace Snk {

  class Apple {

  public:
    Apple(const float posX, const float posY, const float size);

    ~Apple();

    float getPosX() const;
    float getPosY() const;
    float getSize() const;

  private:
    float mPosX;
    float mPosY;
    float mSize;
  };

}