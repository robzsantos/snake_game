#pragma once

#include "SnakeBodyParts.h"

namespace Snk {

  class SnakeTail : public SnakeBodyParts {

  public:
    SnakeTail(const int row, const int column, const int size, 
      const SnakeDirection direction);

    ~SnakeTail();

    void move(SnakeBodyParts *previous = nullptr);

  };

}
