#pragma once

#include <vector>

#include "../Constants.h"
#include "Lot.h"

namespace Snk {

  class Terrain {
  
  public:
    Terrain(const float width, const float height, const float lotSize,
            const TerrainSize tSize);

    ~Terrain();

    std::vector<std::vector<Lot>> &getLots();

    int getNumLotsX() const;

    int getNumLotsY() const;

    float getOffsetX() const;

    float getOffsetY() const;

    Lot &getLotFree();

    void cleanLot(const int row, const int column);

    void createTerrain(const TerrainSize tSize);

  private:
    int numLotsX;
    int numLotsY;

    float mOffsetX;
    float mOffsetY;
    float mLotSize;

    std::vector<std::vector<Lot>> mLots;

    sf::Vector2f mTerrainTotalSize;
  };

} // namespace Snk
