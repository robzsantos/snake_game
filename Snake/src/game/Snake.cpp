#include "Snake.h"

namespace Snk {

  Snake::Snake(const int startRow, const int startCol) {

    resetSnake(startRow, startCol);
    mLifes = GAME_SNAKE_START_LIFE;
  }

  Snake::~Snake() {}

  void Snake::decreaseLife() { mLifes--; }

  void Snake::setSpeed(const int value) { mSpeed = value; }

  int Snake::getLifes() const { return mLifes; }

  int Snake::getSpeed() const { return mSpeed; }

  bool Snake::isWaitingMove() const { return mWaitingMove; }

  void Snake::resetSnake(const int startRow, const int startCol) {
    mHead = std::make_unique<SnakeHead>(
      startRow - 2, startCol, GAME_SNAKE_BODY_PART_SIZE, SnakeDirection::Up);
    mTail = std::make_unique<SnakeTail>(
      startRow, startCol, GAME_SNAKE_BODY_PART_SIZE, SnakeDirection::Up);

    mBody.clear();
    mBody.push_back(
      std::move(m_createBody(startRow - 1, startCol, SnakeDirection::Up)));

    mBodyLength = GAME_SNAKE_INIT_LENGTH;
  }

  void Snake::setNextPositionByLot(Terrain *terrain) {

    Lot *lot;
    SnakePart bodyPart;

    for (int i = 0; i < getBodyLength(); i++) {

      if (i == 0) {
        lot = &(terrain->getLots()[mHead->getRow()][mHead->getColumn()]);
        bodyPart = SnakePart::Head;
      }
      else if (i == getBodyLength() - 1) {
        lot = &(terrain->getLots()[mTail->getRow()][mTail->getColumn()]);
        bodyPart = SnakePart::Tail;
      }
      else {
        lot = &(terrain->getLots()[mBody[i - 1]->getRow()]
          [mBody[i - 1]->getColumn()]);
        bodyPart = SnakePart::Body;
      }
      lot->setOccupation(LotOccupation::Snake);
      setPosition(lot->getPosX(), lot->getPosY(), bodyPart, i - 1);
    }
  }

  void Snake::setPosition(const float posX, const float posY, const SnakePart part,
    const int currentPos) {

    switch (part) {
    case SnakePart::Head:
      mHead->setPosition(posX, posY);
      break;
    case SnakePart::Body:
      mBody[currentPos]->setPosition(posX, posY);
      break;
    case SnakePart::Tail:
      mTail->setPosition(posX, posY);
      break;
    }
  }

  void Snake::changeDirection(SnakeDirection direction) {
    mHead->changeDirection(direction);
    mWaitingMove = true;
  }

  void Snake::move() {

    mHead->move();

    mBody[0]->move(mHead.get());
    for (unsigned int i = 1; i < mBody.size(); ++i) {
      mBody[i]->move(mBody[i - 1].get());
    }

    mTail->move(mBody.back().get());

    mWaitingMove = false;
  }

  void Snake::increaseBody() {

    std::unique_ptr<SnakeBody> newBody =
      m_createBody(mTail->getRow(), mTail->getColumn(), mTail->getDirection());

    newBody->setPreviousCoordinates();
    mBody.push_back(std::move(newBody));
    mBodyLength++;
    mTail->move();
  }

  bool Snake::fadeOut() const {

    const int decreaseValue = 3;
    bool isFadingOut = false;

    if (mHead->getAlpha() > 0) {
      mHead->fadeOut(decreaseValue);
      mTail->fadeOut(decreaseValue);
      for (auto &body : mBody) {
        body->fadeOut(decreaseValue);
      }
      isFadingOut = true;
    }

    return isFadingOut;
  }

  std::vector<std::unique_ptr<SnakeBody>> &Snake::getBody() { return mBody; }
  std::unique_ptr<SnakeHead> &Snake::getHead() { return mHead; }
  std::unique_ptr<SnakeTail> &Snake::getTail() { return mTail; }
  int Snake::getBodyLength() const { return mBodyLength; }

  std::unique_ptr<SnakeBody> Snake::m_createBody (const int row, const int column, 
    const SnakeDirection direction) const {

    return std::unique_ptr<SnakeBody> (new SnakeBody(row, column, 
      GAME_SNAKE_BODY_PART_SIZE, direction));
  }

} // namespace Snk
