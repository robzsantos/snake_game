#include "SnakeBody.h"

namespace Snk {

    SnakeBody::SnakeBody(const int row, const int column, const int size, 
      const SnakeDirection direction) : SnakeBodyParts(row, column, size, direction) {}

    SnakeBody::~SnakeBody() {}

    void SnakeBody::move(SnakeBodyParts *previous) {

      mPrevCol = mCurrentCol;
      mPrevRow = mCurrentRow;
      mPrevDirection = mCurrentDirection;

      if (previous) {
        mCurrentCol = previous->getPrevColumn();
        mCurrentRow = previous->getPrevRow();
        mCurrentDirection = previous->getPrevDirection();
      }
    }

    void SnakeBody::setPreviousCoordinates() {

      switch (mCurrentDirection) {
      case SnakeDirection::Left:
        mPrevCol = mCurrentCol + 1;
        mPrevRow = mCurrentRow;
        break;
      case SnakeDirection::Up:
        mPrevRow = mCurrentRow + 1;
        mPrevCol = mCurrentCol;
        break;
      case SnakeDirection::Right:
        mPrevCol = mCurrentCol - 1;
        mPrevRow = mCurrentRow;
        break;
      case SnakeDirection::Down:
        mPrevRow = mCurrentRow - 1;
        mPrevCol = mCurrentCol;
        break;
      }
    }

}
