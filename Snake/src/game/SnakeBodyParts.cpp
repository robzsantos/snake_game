#include "SnakeBodyParts.h"

namespace Snk {

  SnakeBodyParts::SnakeBodyParts(const int row, const int column, const int size, 
    const SnakeDirection direction)
    : mCurrentRow(row), mCurrentCol(column), mSize(size),
    mCurrentDirection(direction), mPrevDirection(direction), mAlpha(255) {}

  SnakeBodyParts::~SnakeBodyParts() {}

  int SnakeBodyParts::getRow() const { return mCurrentRow; }
  int SnakeBodyParts::getColumn() const { return mCurrentCol; }
  int SnakeBodyParts::getPrevRow() const { return mPrevRow; }
  int SnakeBodyParts::getPrevColumn() const { return mPrevCol; }
  int SnakeBodyParts::getAlpha() const { return mAlpha; }
  int SnakeBodyParts::getSize() const { return mSize; }
  float SnakeBodyParts::getPosX() const { return mPosX; }
  float SnakeBodyParts::getPosY() const { return mPosY; }
  SnakeDirection SnakeBodyParts::getDirection() const { return mCurrentDirection; }
  SnakeDirection SnakeBodyParts::getPrevDirection() const { return mPrevDirection; }

  void SnakeBodyParts::setPosition(const float posX, const float posY) {
    mPosX = posX;
    mPosY = posY;
  }

  void SnakeBodyParts::fadeOut(const int decreaseValue) {
    mAlpha -= decreaseValue;

    if (mAlpha <= 0) {
      mAlpha = 0;
    }
  }

} // namespace Snk