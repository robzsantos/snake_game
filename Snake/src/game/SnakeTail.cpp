#include "SnakeTail.h"

namespace Snk {

  SnakeTail::SnakeTail(const int row, const int column, const int size, 
    const SnakeDirection direction)
    : SnakeBodyParts(row, column, size, direction) {}

  SnakeTail::~SnakeTail() {}

  void SnakeTail::move(SnakeBodyParts *previous) {

    if (previous) {
      mPrevCol = mCurrentCol;
      mCurrentCol = previous->getPrevColumn();

      mPrevRow = mCurrentRow;
      mCurrentRow = previous->getPrevRow();

      mPrevDirection = mCurrentDirection;
      mCurrentDirection = previous->getPrevDirection();
    }
    else {
      mCurrentCol = mPrevCol;
      mCurrentRow = mPrevRow;
      mCurrentDirection = mPrevDirection;
    }
  }

}
