#include "Lot.h"

namespace Snk {

  Lot::Lot(const float posX, const float posY, const float size, const int row,
    const int col)
    : mSize(size), mPosX(posX), mPosY(posY), mRow(row), mColumn(col),
    mOccupation(LotOccupation::Free) {}

  Lot::~Lot() {}

  int Lot::getRow() const { return mRow; }
  int Lot::getColumn() const { return mColumn; }
  float Lot::getPosX() const { return mPosX; }
  float Lot::getPosY() const { return mPosY; }
  float Lot::getSize() const { return mSize; }
  LotOccupation Lot::getOccupation() const { return mOccupation; }

  void Lot::setOccupation(const LotOccupation value) { mOccupation = value; }

}