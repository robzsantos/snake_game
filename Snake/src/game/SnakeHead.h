#pragma once

#include "SnakeBodyParts.h"

namespace Snk {

  class SnakeHead : public SnakeBodyParts {

  public:
    SnakeHead(const int row, const int column, const int size, const SnakeDirection direction);

    ~SnakeHead();

    void move(SnakeBodyParts *previous = nullptr);

    void changeDirection(const SnakeDirection direction);

  };

}
