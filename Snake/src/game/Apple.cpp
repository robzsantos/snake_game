#include "Apple.h"

namespace Snk {

    Apple::Apple(const float posX, const float posY, const float size)
      : mSize(size), mPosX(posX), mPosY(posY) {}

    Apple::~Apple() {}

    float Apple::getPosX() const { return mPosX; }
    float Apple::getPosY() const { return mPosY; }
    float Apple::getSize() const { return mSize; }

}