#pragma once
#include <time.h>

#include "GameLevelRules.h"
#include "GameView.h"
#include "states/StateEndGame.h"
#include "states/StateGameOver.h"
#include "states/StateLevelUp.h"
#include "states/StatePauseGame.h"
#include "states/StateSnakeDying.h"
#include "states/StateSnakeEating.h"
#include "states/StateSnakeMoving.h"
#include "states/StateSnakeRespawn.h"
#include "states/StateStartGame.h"

namespace Snk {
  class GameModel {

  public:
    GameModel(GameView &view);

    ~GameModel();

    void changeSnakeDirection(const SnakeDirection direction);
    void createApple();
    void increaseApples();
    void increaseLevel();
    void increaseSpeed();
    void onPauseGame();
    void onContinueGame();
    void refresh();
    void resetGame();
    void resetTerrain();
    void resetApple();
    void setGameOver();

    bool canMove();
    int getLevel() const;
    int getCurrentBodyLevel() const;
    GameView &getView() const;
    Snake *getSnake() const;
    Terrain *getTerrain() const;

    States getCurrentState() const;
    States getPreviousState() const;
    void setNextState(const States state);

  private:
    GameView &mView;
    sf::Clock mClock;

    std::unique_ptr<Terrain> mTerrain{nullptr};
    std::unique_ptr<Snake> mSnake{nullptr};
    std::unique_ptr<Apple> mApple{nullptr};

    int mLastElapsedTime;
    int mApplesEaten;
    int mLevel;

    State *mCurrentState;
    States mCurrentStateEnum;
    States mPreviousStateEnum;

    std::unique_ptr<StateStartGame> mStateStartGame;
    std::unique_ptr<StateSnakeMoving> mStateSnakeMoving;
    std::unique_ptr<StateSnakeDying> mStateSnakeDying;
    std::unique_ptr<StateSnakeRespawn> mStateSnakeRespawn;
    std::unique_ptr<StateSnakeEating> mStateSnakeEating;
    std::unique_ptr<StateLevelUp> mStateLevelUp;
    std::unique_ptr<StateGameOver> mStateGameOver;
    std::unique_ptr<StateEndGame> mStateEndGame;
    std::unique_ptr<StatePauseGame> mStatePauseGame;

    bool _getTimeToUpdate();
  };
} // namespace Snk
